tags: GNU/Linux IBM
title: Systemd, a piece of shit you should avoid
author: Werwolf
date: 2021-05-09
aside: My personal opinion about why systemd is considered harmful

# Systemd sucks

Yeah, I know that this topic has been discussed since systemd's early days. This is only my
opinion about it. Of course that you are free to use whatever you want. However I'll try to
give you enough arguments to make you think about running away from systemd. 

## Systemd goes against the Unix philosophy

Systemd does not comply with the Unix philosophy, which is "do one thing and do it well".

Systemd's responsibilities immensely exceed that of an init. It handles power management, device
management, syslog, mount points, disk encryption, socket API/inetd, network config, login and
session management, readahead, GPT partition discovery, container registration,
hostname, locale and time management and a bunch of other things.

One of the consequences that this brings is that plenty of non kernel upgrades will require a
reboot when using systemd. This was a Windows exclusive feature until now, so cool, isn't it?

There are also a lot of scenarios in which systemd can crash and break our whole OS. This is, of
course, because it is managing too many things.

It seems that the only thing that systemd is still missing is a good init system.

## Privacy issues

Since systemd is developed by Red Hat (owned by IBM) there has been various conspiracy theories
about it being spyware.

A major issue (which is not conspiracy, it has been proved and you can read the code yourself)
		is the fact that [**systemd fallbacks to Google DNS servers**.](https://isc.sans.edu/forums/diary/Systemd+Could+Fallback+to+Google+DNS/22516/) This is a severe privacy issue. Yes, you could change this. But you would have to recompile systemd, which isn't fun at all.

## Systemd is destroying Linux diversity and modularity

Despite all the hate it has received, systemd has been widely adopted. The first major
distribution that started using it was Fedora, Red Hat's community distribution. Since
then, Debian, openSUSE, Arch Linux and thousands of other distros have adopted systemd.
This is devastating for the diverse Linux ecosystem.

What is even worse is that tons of pieces of software have started relying on systemd which
is causing issues for those of us that don't want to use it. For example, **since GNOME 3.8, the
most used desktop environment now requires systemd**. This isn't extrange at all, the GNOME
project has always been Red Hat's dog. Just like GNOME, countless programs need systemd
to work properly. 

### Systemd is incompatible with other Unix like operating system

The BSD family is especially affected by this. Most of the old school Linux inits could be
ported to other Unix like OSs, but with systemd that is impossible. 

On the other hand, this may seem like an advantage since this assures us that we will never
see systemd in any *BSD. I find this quite relieving.

## Systemd is bloated and extremely complicated

Systemd is such a big piece of code that it is now pretty much impossible to understand it's
codebase unless you spend months studying it different parts. This may bother advanced users
who want to understand how their system works. It has more than 1.2 million lines of code. **If
systemd doesn't stop growing soon, we'll have to start calling the OS GNU/Systemd/Linux**

It also affects people who use older hardware. Systemd is bloated. This
can be observed in any distro designed with old hardware in mind. These distros avoid systemd: AntiX, Puppy Linux, Tiny
Core, etc.

### Systemd increases our dependency on corporations

Having a huge codebase makes impossible for one person to maintain it, making necessary a
dedicated team of developers who are in charge of it. **This makes us even more dependant on Red Hat**, which
is the corp that develops systemd. 

## Alternatives to systemd

There are quite a few inits which aren't systemd. I'll list some of them for you:

- Runit
- SysVinit
- OpenRC
- Shepherd
- Cinit
- Upstart
- Minit
- S6
- Hummingbird

### Distros without systemd

Although major distributions had moved to systemd, there are a lot of good distros that come
without systemd. My recommendations are:

- If you're using Debian or a Debian based distribution, you should try **Devuan**, which
aims to be Debian but providing init freedom, giving you the option to choose between SysVinit, OpenRC and Runit.
- For Arch Linux and Manjaro users, you have **Artix**. Artix is an Arch based distro and it can
be installed from the command line (like Arch) or using the Calamares installer (like Manjaro).
		It has various editions with different DEs and WMs. Artix, like Devuan, has various
init to choose from: OpenRC, Runit and S6.
- Other great distro I highly recommend is **Void**. Void is a rolling release distribution, it
uses Runit and you can choose between the glibc and musl versions.
- For freedom lovers, I recommend **GNU Guix**, which is a 100% free (as in freedom) GNU+Linux distribution. It
uses seppherd as the init software.
- If you're an advanced user, you can choose between **Gentoo**, **CRUX** and **Slackware**.
- You may also consider switching to *BSD. I'd recommend **GhostBSD** for new users, although my personal favorites are **FreeBSD** and, especially, **OpenBSD**.

You can find a longer list of non-systemd distros in [Distrowatch](https://distrowatch.com/search.php?ostype=All&category=All&origin=All&basedon=All&notbasedon=None&desktop=All&architecture=All&package=All&rolling=All&isosize=All&netinstall=All&language=All&defaultinit=Not+systemd&status=Active#simple)

## Conclusion

I know that most of the people don't care about their init software. However, it's pretty easy to
escape from systemd, so now that you know about it and it's issues, why wouldn't you try a
systemd-free distro?
