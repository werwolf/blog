tags: GNU/Linux WindowManager
title: Ratpoison: Kill the rat
author: Werwolf
date: 2021-04-24
aside: A guide about configuring the Ratpoison window manager and explaining my personal config.

# Table of Contents:

* [Starting ratpoison](#Starting ratpoison)
* [My ratpoison config](#My ratpoison config)
* [Useful resources that you may want to read while playing with your .ratpoisonrc file:](#Useful resources that you may want to read while playing with your .ratpoisonrc file:)

After being window manager hopping for a while, I think that I've found my new home: Ratpoison. It's a minimalist window manager that was build from GNU screen, a terminal multiplexer. It has no big dependencies and it's the most lightweight wm that I've ever tried. It's RAM consumption is even lower than Fluxbox's or Windowmaker's, at least in my machine. That's awesome, isn't it? 

I've used quite a variety of window managers: i3, openbox, jwm, qtile, icewm, xmonad and lately, dwm. Every wm (except dwm) of that list has a ton of function that I'd never use. Why would I want a lot of functionality when all that my wm has to do is tiling my windows? Don't get me wrong, ratpoison isn't that feature poor. In fact, it's highly extensible through scripts. There's, for example, a script that creates workspaces, like i3's or dwm's. That's the only one that I currently use, I want my workflow to be as minimal as possible.

Ratpoison can be a bit confusing if you have never used it. The mod key or super key in ratpoison is called the escape key. It's Ctrl + t. You'll see then that your cursor is now a white square. This means that you've pulsed the escape key. You can now use any of the keybindings that come in the default config. Pulse ? if you want to see the list. 

If you want to hear a ratpoison's con, I'd say it default bindings. Unless you have used GNU Screen before (in that case the keybindings will be the same) you have to get used to it. I think that the combination of Ctrl and t is a bit annoying so I'd recommend you changing it to a more comfortable key combination. Eg I've changed my escape key to Caps Lock. I'll explain you how to do this later in this post. The other keybindings are not that bad. I've changed them anyway, because I'm a vi person and I like to use the vimkeys everywhere I can. My window manager is not the exception. 

# Starting ratpoison

If you're using a display manager, like gdm or lightdm it's possible that ratpoison won't appear between your different options. Display managers source the available sessions from /usr/share/xsessions/ where most desktops environments and window managers create .desktop files. However ratpoison creates a .desktop file in /etc/X11/sessions/ instead. So if you want your DM to find ratpoison, you have to copy the file /etc/X11/sessions/ratpoison.desktop to /usr/share/xsessions/ratpoison.desktop. After doing that, your display manager should succesfully show ratpoison as an option.

If you'd like to use the .xinitrc file instead, you only have to add this line:

~~~shell
#fire up ratpoison!
exec /usr/bin/ratpoison
~~~

# My ratpoison config

I'm not knowledgeable at all in configuring ratpoison, but I've managed to build a config file that works for my workflow. I'll explain you what does it consist on, so you can write your own. 

You'll have to create your .ratpoisonrc in your home directory. This is a simple script that will be executed when you start ratpoison. 

~~~shell
#set escape to caps lock thanks to .xmodmaprc and .xinitrc
escape F13
~~~

The first part of my config is really simple. It changes my escape key to F13. Yeah, I know what you're thinking. There's no F13. Well, this is a simple workaround that I found to use the Caps Lock key as my escape key. To do this you'll have to create a file called .xmodmaprc in your home directory. Then, add these lines to it:

~~~shell
remove lock = Caps_Lock
keycode 66 = F13
~~~

You'll need to add this to your .xinitrc (this have to go before executing ratpoison)

~~~shell
xmodmap .xmodmaprc
~~~

The next part of my .ratpoisonrc consist on creating a keybinding for restarting ratpoison and a script, which makes the focus follow your mouse. This script comes with ratpoison, you just need to comment out this line and it will work. However I don't like that feature. 

~~~shell
#restart ratpoison
bind R restart

#focus follows mouse script for ratpoison
#exec /usr/share/ratpoison/sloppy
~~~

The next part lets you configure how to move between windows and how to split frames. 

~~~shell
#moving windows
unbind k
bind j focusdown
bind h focusleft
bind k focusup
bind l focusright
bind C-j exchangedown
bind C-h exchangeleft
bind C-k exchangeup
bind C-l exchangeright
unbind c
bind c delete
unbind C
bind C kill

#split frames
bind S split
bind s hsplit
bind u undo
bind q remove
bind o only

#resizing frames
unbind r
bind H resize -15 0
bind L resize 15 0
bind J resize 0 -15
bind K resize 0 -15
~~~

There's a line that turns off ratpoison's startup message. Then you have a few lines that you'll need if you want to use xscreensaver as your screensaver. I am not currently using it but just in case.

~~~shell
#The first line makes sure any running xscreensaver is killed.
#The second starts and backgrounds xscreensaver
#exec xscreensaver-command -exit
#exec xscreensaver &
~~~

It's the time to create workspaces. You can change the number of workspaces to create and how to access them. It's as simple as changing these lines.

~~~shell
#this creates 6 workspaces, to swap between them use Alt+F1, Alt+F2, etc.
exec /usr/bin/rpws init 6 -k
~~~

You can also put your favorite wallpaper as the background, instead of a black screen. I'm using nitrogen for this. Note that for using this command it's required that you have previously chosen a wallpaper with nitrogen.

~~~shell
#restoring wallpaper using nitrogen
exec nitrogen --restore
~~~

Finally, you may want to create some keybindings that launch your favorite programs. One that is specially useful is dmenu, because from dmenu you can launch pretty much every program that is in your system. I also have keybindings for other programs: firefox, pcmanfm, etc.

~~~shell
#bindings for programs
bind d exec dmenu_run
bind space exec sakura
bind f exec firefox
bind p exec pcmanfm
bind v exec sakura -e nvim
~~~

Now you should have your configuration file ready. So you just need to open ratpoison and start playing around. Enjoy it, it's a great window manager!

BTW, if you want to copy my whole .ratpoisonrc, you can have a look at my git repository, I'll try to maintain everything well explained: https://codeberg.org/werwolf/initdotfiles.git

### Useful resources that you may want to read while playing with your .ratpoisonrc file: 

- http://ratpoison.wxcvbn.org/cgi-bin/wiki.pl/ChangeEscapeKeyHOWTO
- https://wiki.gentoo.org/wiki/Ratpoison#Startup_file
- https://www.nongnu.org/ratpoison/doc/index.html
- https://www.antixforum.com/forums/topic/howto-ratpoison-basic-configuration/
- https://thinkmoult.com/ratpoison-an-efficient-and-minimalist-wm.html
