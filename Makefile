.SUFFIXES: .md .xml

# If you want to use `make installwww` to publish your blog, put the
# publication point here.  I'll usually use `make` to put it all
# together, review, then `doas make installwww` to publish.

WWWDIR	 = ~/www

# Add articles here.  For simplicity with linking `index.html` to the
# newest one, the order is publication order.
#
#
# TODO:
# separate posts and images on different directories to have a cleaner root folder

ARTICLES = ratpoison.xml \
	   you-dont-need-a-new-computer.xml \
	   systemd-sucks.xml \
	   firefox-hardening-guide.xml \
	   brave-is-shit.xml \
	   android-privacy-guide.xml \
		 on-blockchain.xml \
		 blog-update.xml \
		 palemoon-hardening.xml

SBLG	 = sblg

all: index.html atom.xml

installwww: all
	mkdir -p $(WWWDIR)
	mkdir -p $(WWWDIR)/images
	mkdir -p $(WWWDIR)/pages
	install -m 0444 atom.xml *.html *.css $(WWWDIR)
	install -m 0444 images/*.png images/*.gif images/*.svg images/apes/*.png $(WWWDIR)/images
	install -m 0444 pages/*.html pages/*.txt *.css $(WWWDIR)/pages

index.html: $(ARTICLES) template.xml index.xml
	$(SBLG) -s cmdline -t template.xml -L $(ARTICLES)
	$(SBLG) -s rcmdline -t index.xml -o $@ $(ARTICLES)

atom.xml: $(ARTICLES) atom-template.xml
	$(SBLG) -s rcmdline -o $@ -a $(ARTICLES)

clean:
	rm -f *.html atom.xml $(ARTICLES)

img:
	rm -rf $(WWWDIR)/images/*jpg $(WWWDIR)/images/*.png $(WWWDIR)/images/*gif

.md.xml:
	echo "<article data-sblg-article=\"1\"" >$@
	echo " data-sblg-tags=\"`lowdown -X tags $<`\">" >> $@
	echo " <header>" >>$@
	echo "  <h2>`lowdown -X title $<`</h2>" >>$@
	echo "  <aside>`lowdown -X aside $<`</aside>" >>$@
	echo "  <address>`lowdown -X author $<`</address>" >>$@
	echo "  <time datetime=\"`lowdown -X date $<`\">`lowdown -X date $<`</time>" >>$@
	echo " </header>" >>$@
	lowdown $< >>$@
	echo "</article>" >>$@
