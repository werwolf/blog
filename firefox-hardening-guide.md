tags: Privacy Firefox Browsers
title: Firefox Hardening Guide
author: Werwolf
date: 2021-05-25
aside: A Firefox hardening guide for retaining your privacy

**UPDATED 3-01-2022**
This guide was outdated for a while, so here you have the update.

There are various similar guides on other sites, but many of these guides were partially incomplete, so I've tried to write the most complete guide as possible, which can be used by paranoid users like me. This is the best way to enhance Firefox. I must remind you that **our goal here is privacy**. If you want to browse the web anonymously, I'd suggest you looking
at Tor instead. There are various similar guides on other sites, but many of these guides were partially incomplete, so I've tried to write the most complete guide as possible, which can be used by people who really want privacy.

This is the best way to enhance Firefox. I must remind you that **our goal here is privacy**. If you want to browse the web anonymously, I'd suggest you looking at Tor instead.

This guide tries to be user-friendly so I won't recommend some more advanced things like uMatrix, 
even if it outclass uBlock Origin. If you're a technical user and want more advanced privacy features
I'd post a hardening guide for a more aproppiate browser, Pale Moon.

# Librewolf

For not advanced users, tweaking Firefox may seem too difficult. It isn't, but I understand that
most of the people don't want to bother with changing their userjs. **[Librewolf](https://librewolf.net/)**
is Firefox but private out of the box. It comes with uBlock Origin and with Mozilla's telemetry removed.



# Vanilla Firefox

The easiest and fastest way to get a private configuration for Firefox is using a custom
userjs which disables Mozilla's telemetry and that 

# Creating a new profile that uses the user.js

I'll assume that you want the maximum level of privacy and guide you through the necessary steps.
If you don't want all the tweaks that the userjs provides, I'd suggest you using Librewolf instead.

You'll have to go to about:profiles and create a new profile. I'll name it "hardened" but
you can call it wathever you want. Then, download user.js from their repo and
unzip it. The result should be a directory called "user.js-88.0" (NOTE: the name
may vary in newer Firefox versions, but the procedure is exactly the same)

You'll have to copy the resulting directory to
/home/"your-user-name"/.mozilla/firefox (in unix-like systems, idk about windows). After that,
go to your .mozilla/firefox directory and
look for a directory that should be named something like "xxxxxxxx.hardened", in my case it was
"bvorhi84.hardened". Now, copy that name and delete the directory.

The last step is to rename the directory "user.js-88.0" with the name of the previously deleted
directory, "bvorhi84.hardened" for me.

Now you can open about:profiles again and select your recently created profile ("hardened"
in my case). Open it and
you should set it as the default profile so it's opened every time that you launch Firefox.

Congratulations! We have already completed the most difficult part of the process.

# Use a privacy friendly search engine

This is the simplest part of the guide. You only have to replace Google with a privacy
respectful search engine. I'll list you the options.

### [Searx](https://searx.me/) 
**A privacy respecting, fully free (as in freedom), metasearch
engine**. It's selfhostable so you can use your own instance or one of the [public
ones](https://searx.space/). I'd avoid Cloudflared ones.

Less recommendable options are:

- [Mojeek](https://www.mojeek.com/): independent search engine based in the UK that says that it doesn't track their users (non-free).
- [YaCy](https://yacy.net/): a libre, peer-to-peer search engine. It's powered by it's users and it doesn't have any central server. This is a unique and great idea, although it doesn't work great.
- [Metager](https://metager.org/): another free metasearch engine runned by a non-profit based in Germany. It's preferrable over Duckduckgo but not over SearX.
- [Duckduckgo Lite](https://html.duckduckgo.com/html/): Duckduckgo but without JavaScript so they can track you as little as possible (Duckduckgo shouldn't be trusted).

# Privacy addons

This section is divided in two parts: the must-haves addons and some recommendations that
will improve your privacy.

## Must-haves

### [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
It's **an efficient blocker that is easy on memory and it's completely free software**. It also has
various modes and it allows for extended blocking similar to NoScript. 

Properly configured, UBO will be our best aliased against ads, trackers and
analytics.

I'd suggest you enabling the advanced mode. It's highly recommended to turn off JavaScript by
default. You can enable it for certain sites whenever you need it. **Blocking JavaScript by default is
probably the best thing we can do to preserve our privacy**.

Learning how to use the advanced mode in uBlock is IMO worth it and highly rewarded if you
want to gain privacy. It isn't that hard and there are tons of tutorials, so that's up to
you.

PS: **uMatrix completely outclass UBO**, so if you want to go beyond UBO's advanced mode, try uMatrix.

### [LocalCDN](https://addons.mozilla.org/en-US/firefox/addon/localcdn-fork-of-decentraleyes/)
LocalCDN is a fork of the well-known Decentraleyes. It emulates Content Delivery Networks
locally by intercepting requests, serving them locally. It's better than Decentraleyes in the
sense that it provides custom rules to use inside uBlock Origin, so these addons work better
together. 

We can find these prepared rules clicking the addon icon, going to advanced and scrolling down.
We select uBlock Origin and copy the rules. Then, we have to go to uBlock, enable "I am an
advanced user", then go to "My rules" and you have to paste the rules on the list at the
right. Then save and commit the changes so they become permanent. You can now forget about
LocalCDN, it'll just work.

### Password Manager
You should be using a trustable password manager for creating and storing your passwords instead of
Firefox's default one or memorizing weak passwords yourself.

**I wouldn't want my passwords to be stored on someone else
servers somewhere, fortunately we have [KeePassXC](https://keepassxc.org/)** a free, as in freedom, 
and off-line password manager. It has the advantage that **your passwords are only stored in a local, strongly encrypted
database** so they won't ever leave your computer if you don't want to. It has addons for
integration with Firefox. You may use
[Syncthing](https://syncthing.net/foundation/) to sync them between your different machines
without any server. 

**My recommendation for people who want sync between their devices would be [Bitwarden](https://bitwarden.com/) 
which is free as in freedom and
free as in free beer!** Yeah, it's both "libre" and "gratis". It has automatic sync between your
devices and it's really easy to use. If you like it and you can afford it, you should buy their
premium membership, which doesn't really provide any necessary feature (their free plan is so
complete) but it's important to support free software projects. You may also self-host it yourself.

The third option (for terminal wizards only) is [GNU Pass](https://www.passwordstore.org/), which is a
**simple password manager that follows the Unix philosophy**. Passwords live in ~/.password-store
**encrypted with your GPG key**. It doesn't provide any integration with Firefox but It has a handy dmenu
script which is very comfortable to use.

## Recommended addons
These are addons that are generally recommended but that in contrast with uBlock or LocalCDN,
they require some maintenance (not so much, actually).

Take into account that some of these addons may be redundant with each other and that
when you have JavaScript enabled, they could fingerprint you by the addons you use, so **I'd
recommend to use the minimum number of addons as possible**, without sacrificing
important privacy features. So you'll have to find the perfect balance between the number of
addons and the advantages they provide.

### [Cookie AutoDelete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/)
This addon deletes cookies everytime that we close a tab or we exit. But it can do much
more, like **cleaning local storage, cleaning on domain change, deleting cache, white and
greylisting, cleaning on domain change**, etc.

### [ClearURLs](https://addons.mozilla.org/en-US/firefox/addon/clearurls/)
It will **automatically remove tracking elements from URLs** (this is a commonly used strategy to
track you) and it's really simple to use. It isn't a must-have because very few times may break
a site that won't work if you clean the URL. But if you notice this, you only have to
temporarily disable the addon. Easy, isn't it?

### [ETag Stoppa](https://addons.mozilla.org/firefox/addon/etag-stoppa/)
It prevents your browser from storing entity tags by removing ETag response headers without
exceptions. It makes a great team with Cookie AutoDelete.

### [CanvasBlocker](https://addons.mozilla.org/firefox/addon/canvasblocker/)
CanvasBlocker is the perfect addon for those momments when you have to enable JavaScript. **It
prevents websites from using some JavaScript APIs to fingerprint you**. It has various levels
and it's really helpful if you want to spoof your fingerprint. They say that it may
break some sites although I've never experienced any issue with this addon. It just works.

### [xBrowserSync](https://addons.mozilla.org/firefox/addon/xbs/)
**Bookmark sync as it should be: end-to-end encrypted and anonymous**. There are different
servers and you can even selfhost it yourself.

### [AdNauseam](https://addons.mozilla.org/en-US/firefox/addon/adnauseam/) (alternative to uBlock)
AdNauseam not only blocks ads, it obfuscates browsing data to resist tracking by the online ad
industry. To throw ad networks off your trail **AdNauseam “clicks” blocked and hidden ads, polluting your data profile and injecting noise into the economic system that drives online
surveillance**. It uses uBlock as it's base, so you also get everything that uBlock is capable of.

### [Privacy Redirect](https://addons.mozilla.org/en-US/firefox/addon/privacy-redirect/)
Redirects Twitter, YouTube, Instagram, Reddit & Google Maps requests **to privacy friendly and libre
alternatives** (Nitter, Invidious, OpenStreetMap, Teddit). It also supports custom
servers so you can use it with your selfhosted instances! **For more advanced users, use
[Redirector](https://addons.mozilla.org/en-US/firefox/addon/redirector/) instead**.

# End of the journey

Your browser should be private by now, so enjoy your freedom.
