tags: Privacy PaleMoon UXP Browsers
title: Pale Moon Hardening Guide
author: Werwolf
date: 2022-03-08
aside: A Pale Moon hardening guide for being a ghost on the internet

Note: This guide was first made as a contribution for [Kill-9](http://kill-9.xyz/), I invite
you to join us on #kill-9 in irc.rizon.net.
I've updated it here because I always forgot to write for the blog, but you should keep an eye on
kill-9 for more privacy and programming stuff.

*DISCLAIMER: this guide is targetted at **power users**. Some sites might break and you'll have to
unbreak them yourself*

Pale Moon is a fork of the last good version of Firefox, before it's painful and slow decline. This
guide was written for Pale Moon 29, but I'm sure that it will remain useful for future versions.

### Pale Moon pros

* Independent from Google, Mozilla and Apple. It's a fork from an old Firefox version that has become it's own thing and has nothing to do with Mozilla anymore. With the concerning Chromium's absolute domination of the browser market share and Mozilla's controlled opposition, Pale Moon is one of the very few truly independent browsers out there.
* No WebRTC Support. WebRTC might be useful sometimes, but people who care about privacy have it disabled anyway because it leaks your IP when using VPNs or Tor.
* Smaller codebase than Chromium or Firefox.
* No DRM at all.
* The superior XUL based extensions. I don't need sandboxing for extensions. I can take care of my own security and check if the addon is doing shady stuff. I don't want addon sandboxing, because it makes them far less flexible and powerful. XUL based extensions are incredibly powerful, Chrome (and modern Firefox) extensions are a toy. Reducing addon capabilities doesn't increment security, the threat from malicious pages is greater than addons.
* No extra bloat. Eg there's no PDF reader built-in. There are addons for that if you need one, but I'd rather leave that to my prefered local PDF reader.

### Pale Moon cons

Of course that Pale Moon is far from perfect:

* The development team is small.
* The main dev hates Tor. (We can use Pale Moon with Tor anyway, as I'll show later).
* The devs decided to block certain addons (It's easy to disable that blocklist tho)
* The main dev has been hostile in the forum at some points.

Luckily, these cons become completely irrelevant comparing them with either Google's or Mozilla's.
You can just use Tor in Pale Moon and set it to ignore the addon blocklist.

Even if the devs were complete idiots, Pale Moon and UXP are the only decent browsers for the modern
web, which is sad but there's no other real alternative.

# Disabling Automatic Connections

By default, Pale Moon makes very little automatic connections in comparison with any so-called
modern browser, but still we need to disable them before hardening it for privacy. It only takes a few
easy steps:

*(you may disable internet connection in your OS so Pale Moon doesn't make any undesirable connection
before you disable them)*

* Right click in the search bar and disable search suggestions, so everything you write is kept off-line until you press enter.
* Change the default homepage to anything else. Tools > Preferences > General and change it to whatever you use. I'd recommend a custom one in a local file, without external connections.
* Disable automatic update checking in Tools > Preferences > Advanced > Update and select "Never check for updates"
* Go to the about:config and disable the addon blocklist (which blocks addons that the devs don't like): 
extensions.blocklist.enabled 	False
* Finally, again the about:config, disable OCSP queries and geolocation:

~~~
services.sync.prefs.sync.security.OCSP.enabled 	False  
security.OCSP.GET.enabled 	False  
security.OCSP.require 	False  
security.OCSP.enabled 	0  
geo.enabled 	False
~~~

By now, Pale Moon won't make any connection without user consent. This process is far easier and
faster than doing the same on Firefox/whatever chromium reskin of your preference.

# Commentary on fingerprinting

A common strategy to avoid fingerprinting is blending in with the majority of users. Most privacy
guides follow this strategy: they tell you to leave your mainstream browser as barebones and with
default options as possible to have a very common fingerprint. In fact, the Tor browser is following
the same strategy and they tell you not to install addons nor change any setting in the browser.

This technique may be effective or not, but it's a pain in the ass. Eg, browsing the clearnet in the Tor
browser without uMatrix is really annoying. They tell you to reduce the number of addons to a
minimum even in vanilla Firefox. Thanks, but no. 

We're accomplishing privacy by getting an unique identity everytime. Read 
[Moonchild's](https://forum.palemoon.org/viewtopic.php?t=22067#p166663) fantastic
commment (Moonchild is the main Pale Moon dev, I don't agree with most of his opinions but
he's right on this)

> By the way, I make my browser fingerprint unique ON PURPOSE and present a different unique fingerprint every time. That, IMO, is the only way to combat fingerprinting and tracking by fingerprint. Smudging won't help because the way around it is simply to come up with another variable to check that is unique. What tracking needs is a unique enough fingerprint that doesn't change otherwise. Smudging might make you briefly less identifiable because you'll be part of a slightly less unique group, and might make you feel better, but isn't actually effective. The recent common-sense documentation that trying to hide by disabling APIs being counterproductive only underlines my point that unique is good, as long as you are a different entity every time.

> As explained before, this also hurts the trackers directly because they will get nonsensical or useless data in their databases from unique fingerprints that will never be seen again; and it will be impossible (or at least very difficult) to distinguish valid-but-fake fingerprints from valid-and-real ones.

# Config privacy tweaks

## Enable canvas poisoning

Remember Moonchild's comment I quoted before? In the very same [forum thread](https://forum.palemoon.org/viewtopic.php?t=22067#p166663)
he tells us about the about:config "canvas.poisondata" option. Go to the about:config and set it to
true. Now, **everytime you reload a page or open a new tab, your canvas hash will be different**. Check it here https://browserleaks.com/canvas
if you want. 

Later, with the help of extensions, we will spoof more identifiable browser data to get
a completely different fingerprint every page reload, not only canvas hash. But it's a great first
step.

## Search Engine

This topic is long enough to write another article, but I'll be brief:

The only decent option which is both private and has decent quality results is **[SearX](https://searx.space/)**.
**Avoid Duckduckgo, Brave search, Startpage and similar fake privacy initiatives**.

Choose a SearX instance that isn't [Cloudflared](https://git.nixnet.services/you/stop_cloudflare)
and if possible hosted by someone you trust.

Tip: I couldn't easily install search engines on Pale Moon, so I used [this addon](https://github.com/tarakbumba/add-as-search-engine-for-palemoon) 
which lets you just right click on any search bar and add it as a Search Engine. You may
remove it after adding your favorite search engines.

## Password Manager

Don't use the built-in password/login manager. Use a trusted one like [pass](https://www.passwordstore.org/)
or [KeePassXC](https://keepassxc.org/). 

Neither of them have direct integration with Pale Moon, but Pass has a dmenu script which can be used to securely copy
passwords, which is very handy. There isn't any KeePass addon for Pale Moon yet, you will have to manually copy the
passwords.

## Disabling WebAssembly

WebAssembly is turning
your browser into another OS, literally compiling other languages into "browser assembly" to execute all
kinds of "apps" (spyware, malware, vulnerabilities). If JavaScript is a privacy nightmare,
this is the fucking hell. WebAssembly? No, thanks. 

*I know that there are fair uses for
both JS and WASM, but they're used to track users. You should be using native programs anyway, which
don't need another operating system on top of your operating system to run. Modern browsers are
memmory hogs, bloated and unmaintainable pieces of software.*

Go to Tools > Preferences > Content > JavaScript and disable WebAssembly. I don't know why Pale Moon
comes with this enabled by default, but it's better to leave it disabled.

# Privacy Extensions

A lot of guides recommend to have as less addons as possible, because you may install malicious
addons or you may be fingerprinted due to the addons that you use. While these reasons seem
plausible, let me tell you two things:

* The addons that we will install are **free software**, so you can audit the code yourself. This doesn't guarantee that they aren't malware, of course. But at least the code is auditable. They are addons that are generally trusted by the community. I've used Mitmproxy to analyze Pale Moon's connections with this addons and there isn't anything weird. If you remain skeptic (which is great), audit the code yourself and report your results. 
* I've tested this Pale Moon setup in multiple fingerprinting tests, browser leaks sites, etc both with JS enabled and disabled. **None of them was able to detect any extension**. Try it yourself if you want.

Overall, extensions (especially XUL ones) transform our browser into **a power tool, offering unlimited possibilities with
customization, privacy, usability and security**. I highly recommend using them, following basic
security practices, of course.

## nMatrix

[nMatrix](https://addons-legacy.palemoon.org/addon/ematrix/) is a fork of uMatrix for Pale Moon,
which btw is in active development for those that think that
uMatrix shouldn't be used because it was deprecated.

nMatrix/uMatrix is **the ultimate content blocker**. **[uBlock Origin advanced mode can't replace
nMatrix](https://digdeeper.her.st/ghost/addons.html#umatrix)**, nMatrix is far more powerful.

Let's configure nMatrix. Enter the dashboard, which can be opened by clicking the black bar with the
name in the popup. Go to Settings > Privacy. I'd recommend selecting everything but "Spoof HTTP referrer string of third-party requests." (we will spoof HTTP referrers with other extension).

After installing, nMatrix will break a lot of websites. That's intended. You can easily unbreak them
by pointing and click once you know how to use nMatrix. It's worth investing time in learning it.
Explaining how to use nMatrix is out of the scope of this guide, but here are some resources:

* https://www.electricmonk.nl/docs/umatrix_tutorial/umatrix_tutorial.html
* https://github.com/gorhill/uMatrix/wiki

I'd recommend leaving **JavaScript disabled by default** and enabling it only on trusted sites when
necessary.
JavaScript is the main tool used for tracking, fingerprinting and malware. If a site is well
designed it should be readable without JS anyway. Unfortunately, bad practices like rendering the
content with JS are very common in "modern" web development. Don't worry, later we'll install a
Reader mode addon that will let us read some sites without JavaScript.

## Decentraleyes 

It's a well-known addon that will serve various CDNs locally so you don't need to connect to third
parties while browsing sites that require them.

It'll only work when you allow these CDNs in nMatrix. If you want Decentraleyes to work with
nMatrix, add these rules to nMatrix:

~~~
* ajax.aspnetcdn.com script allow  
* ajax.googleapis.com script allow  
* ajax.microsoft.com script allow  
* ajax.proxy.ustclug.org script allow  
* cdn.jsdelivr.net script allow  
* cdnjs.cloudflare.com script allow  
* code.jquery.com script allow  
* libs.baidu.com script allow
~~~

You can check if Decentraleyes is working here: https://decentraleyes.org/test/ (you'll have to
enable JavaScript)

##  Proxy Privacy Ruler

The [Proxy Privacy Ruler](https://github.com/JustOff/proxy-privacy-ruler) is probably one of my
favorite addons for Pale Moon. We will use it to browse through Tor in Pale Moon. We will configure
to open .onion sites and any site you want always through Tor automatically. And it will make the private mode
actually useful: we'll set it up in order to always **use Tor as a proxy when browsing in private
mode**.

You'll need Tor running in your system, install it and start it on boot. 
On GNU/Linux with a sane init, OpenRC, just run: **rc-update add tor default**. If you're
using systemd: **systemctl enable tor.service**. I'm sure that my beloved *BSD users
know their respective commands.

Download Proxy Privacy Ruler from [their github repo](https://github.com/JustOff/proxy-privacy-ruler)
and drag the file into Pale Moon. You should be asked if you want to install the addon. Accept and
install it.

In Pale Moon, go to Tools > Preferences > Advanced > Network > Connection > Settings. Select "manual
proxy configuration". Leave blank the HTTP, SSL and FTP proxies. The one that you need to modify is
the SOCKS Host. Write the following:
"SOCKS Host: 127.0.0.1			Port: 9050"
Make sure that SOCKS v5 is selected. And near the bottom, check the "Use proxy to perform DNS
queries" box. Done, you may close the Network settings now.

In Tools > Preferences > Privacy set "Use custom settings for history" and **uncheck** "Always use
private mode", "Remember my browsing and download history" and "Remember search and form history".
Check allow sites to store cookies and data, set it to never accept third party cookies (although you can
leave it by default, you can manage this with nMatrix). Set cookies to be cleared when you close
Pale Moon and also check to clear history when Pale Moon closes.

Go to Tools > Addons and search for Proxy Privacy Ruler. Click "Preferences" and check the "Enable",
"Limit Proxy by Private Browsing" and "Limit Proxy by Domain List" boxes. In the Domain list field
write  *.onion;example.com

Now restart your browser, make sure that Tor is running and open two windows, one in normal mode and
one in private browsing. If you go to ipchicken.com on both of them, you should see different IP
addresses: your real IP on the non-private window and a Tor IP on the private browsing window. This
verifies that Proxy Privacy Ruler is working as expected.

Note that even in the non private window, if you visit example.com you'll be doing it through Tor,
since it's on the Domain List. I use this all the time for sites that I don't want to reveal my IP
to, for example, Micro$oft's Github. You may use Private Browsing when you don't want to reveal your
IP to any site. Btw now **you're able to browse .onion sites in Pale Moon**, which is really useful.

## Secret Agent

The [Secret Agent](https://www.dephormation.org.uk/?page=81) extension is the swiss knife for
spoofing your browser. It's a super complete tool for spoofing a lot of things: user agents, accept
headers, JavaScript OSCPU strings, ETags, caching, proxy headers, etc.

I won't explain how to configure everything because it has a ton of options. Experiment with them.
I'll only guide you through the ones that I consider essential. Read [this page](https://www.dephormation.org.uk/?page=81)
for a complete overview of the options.

First of all, for some reason it adds an annoying as hell toolbar. Right click > customize and
drag the addon icon next to the other adddons. Then View > Toolbars and uncheck the Secret Agent
Toolbar.

Now go to the addon preferences. In the first section, I'd recommend selecting the Stealth mode by
default, which is the one which spoofs everything. Below, I've selected that it rotates with every
HTTP request, because that option being the most disruptive is just what we're looking for in order
to get a completely unique fingerprint every page reload. It makes a great team with the poison
canvas data option we set before. If the disruptive rotation breaks a site that you need to browse,
you can just easily whitelist it using the pop up menu.

In the user agent section, you have a large list of user agents. This
list is a bit old, you may search for a new one, add more user agents or even remove it and paste a
long paragraph of your favorite book. Read this, extracted from the [adddon's site](https://www.dephormation.org.uk/?page=81)

> Secret Agent randomises your browser 'User Agent' headers by picking a value from a list. You don't have to use the standard User Agent list. In fact, I'd encourage you to customise the list, to better match (or hide) the general characteristics of the device you use. I normally replace the standard list with 2,000+ desktop user agents.

> Alternatively a simple block of nonsense paragraphs works well... For example, you could use a block of text from Project Gutenberg or a list of Bond Films. Web sites will usually default to a fail-safe 'standards compliant' version of their content when they don't recognise your browser's User Agent headers. More commonly, web sites ignore your User Agent completely.

> On whitelisted sites, you can choose to present the browser's default User Agent, or configure a User Agent override.

> Tip; start with a small list of user agents, and build on it once you understand the effect that randomising your user agent has on your net surfing. For greater stability/ease of use, closely match the list of user agents to your real browser. If you want to conceal the type of browser you use, try a broader range of obscure user agents instead. 

You may apply similar techniques for the following sections: Accept Headers and JavaScript OCSPU
Strings. Add more strings with real information of other browsers or just paste random strings of
text. I don't know which approach is the most effective, but I've tried both of them and both worked
great on fingerprinting tests: they gave you a completely unique fingerprint every page reload,
which is our objective.

In the ETags and caching section, after reading the bottom line and the docs page, I've checked the first box only and
in the Proxy Headers I've checked both boxes.

**In the hijack detection section, uncheck every box**, because they gave me annoying warnings
while browsing and you can already block third party media and content using nMatrix.

In the privacy section > Browser Privacy Tuning Preferences, I'd recommend checking every box but
the first one (we don't want to browse always in private mode since we've configured private mode
separately to use Tor). 

The rest of the options are up to you, I've covered the most important ones
that will provide us with a randomized fingerprint with every reload.

## Canvas Blocker Legacy

I wasn't sure if I should include this one since we're already using the built in canvas poisoning
option, but after testing it on fingerprinting tests sites, I've decided to include it.

While the about:config option randomize our canvas hash, Canvas Blocker fakes other APIs like the
Window API, the DOMRect API and the Audio API.

I'm unsure if this is really effective, but on the tests it seems to work ok with the about:config
option. So it's up to you if you want to use it or not. I'd be glad if anyone more knowledgeable could confirm
this.

## I don't care about cookies

I'm sure that you can get the same effect with nMatrix custom rules, but [I don't care about cookies](https://addons-legacy.palemoon.org/addon/i-dont-care-about-cookies/)
blocks cookies banners out of the box. It's continuosly updated to block cookies banners that aren't
blocked by generic rules on popular sites. One that I really appreciate is that they remove Stack
Overflow's annoying cookie banner which can't be removed without enabling JavaScript.

## URL Rewriter

[URL Rewriter](https://addons-legacy.palemoon.org/addon/url-rewriter/), a Redirector fork,
is a powerful addon to redirect popular websites to it's privacy respecting
frontends: eg Youtube to Invidious and Twitter to Nitter. You have to create your own rules. Read
the help section for a quick explanation.

## Reader Mode

**The fantastic Firefox Reader Mode but without Mozilla's telemetry**. [Reader View](https://addons-legacy.palemoon.org/addon/url-rewriter/)
works flawlessly for sites that break when blocking JS, for viewing bloated and annoying pages or
even just for reading a blog post with a dark mode if there isn't one available. Tip: In the
preferences of the addon, you can set it to appear in the URL bar, just as in Firefox.

# Conclusion

Your Pale Moon browser should have a completely randomized fingerprint every page reload. Go to
every fingerprinting test out there and see how you score very low (they'll say that your
fingerprint is unique) but then if you pass the test again, you'll obtain completely different
results, effectively making useless their trackers. Check these tests sites and observe how your
canvas fingerprint is different every time:

* https://browserleaks.com/canvas
* https://amiunique.org/fp
* https://hidester.com/browser-fingerprint/ (here you may observe how your canvas hash changes with
every reload)

Additionally, you can mask your IP through Tor 
using Private Browsing. I consider this the best browser setup for privacy at the momment.
