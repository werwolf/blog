tags: test
title: About
author: Werwolf
date: 2021-04-30
aside: test


BlackGNU is a blog about freedom. The main topics will usually be free software and anarchy related ideas.

Note that BlackGNU is just the name of this blog. It doesn't have any affiliation with the GNU project. I chose the name because I love free software and GNU's philosophy. This blog is my little contribution to spread it's message. But I'm not part of the GNU project. 

In the unlikely event that someone from the GNU project read this and thinks that I should change the name of the blog, tell me and I'll change it immediately.

## The Author

Well, you may call me Werwolf. It's just nick that I tend to use on the internet. It doesn't mean anything, it's just the title of a song I used to love.

You may ask me for my real name if you want. But use encryption please.

### About me

I love free software and freedom in general. I'm anarchist, so you should expect some politic content from time to time. I'm also a punk and a metalhead. I really love music.

PS. Not trying to look edgy, It's just me. If you think that being punk and anarchist is edgy then fuck off.

## Contact

You can contact me on:

* [The fediverse](https://fosstodon.org/@werwolf)
* By email, on blackgnu [@] riseup [dot] net

## License

Anything written in this site is under the [CC BY SA](../license) license, unless otherwise stated. You're free to use the content on this website under the terms of that license. Images might or might not be under the same license.
