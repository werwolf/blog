tags: Blockchain Web3
title: On blockchain and Web3
author: Werwolf
date: 2022-01-10
aside: A personal opinion about blockchain and the 'web3' fanaticism

*Please, cryptobros refrain from emailing me angry rants. I'll accept any kind of constructive
criticism, of course. Just keep in mind that this is an opinion article*

This article was finished in early January 2022, but it hasn't been published until now because I
was setting up this site and I didn't want to join the rising wave of posts about web3. The events
of the last month about web3 and cryptocurrencies have only made this article even more relevant.

After a while reading articles for and against web3 and doing a bit of research, I've decided to
write my personal conclusion. This isn't going to be a technical analysis of blockchain and
cryptocurrencies, but my personal thoughts about them. I've had to read a lot of economics, a field
which I completely despise, but even then, doing research for this
article was an interesting task. 

First, I'd like to give you some quick definitions:

### What's a blockchain?

A blockchain is a linked list. Imagine a database but instead of being managed by one person or
entity in one server, it is managed by the multiple nodes in a computer network. This database can
record data but the data written in it can't be changed. This is known as immutability.

### What's the web3?

Even web3 supporters can't agree with what web3 is and what it is not. But we'll take what it seems to
be the most accepted definition, given by a cofounder of Ethereum: The web3 is a decentralized online ecosystem built on top of blockchain.

### What's NFT?

NFT (non fungible token) are supposed to be unique digital identifiers on a blockchain.

They are intended to be used by artists to "protect" their work. They can be images,
songs, etc.

# Table of Contents


* [Summary](#Summary)
	* [Web3 is harmful](#Web3 is harmful)
	* [Why is there so much hype if it is a scam?](#Why is there so much hype if it is a scam?)
* [Decentralization](#Decentralization)
	* [Offering a Web 2.0 vs a web3 service](#Offering a Web 2.0 vs a web3 service)
* [Immutability](#Immutability)
* [Environmental impact](#Environmental impact)
	* [Is there any solution?](#Is there any solution?)
		* [What is a consensus mechanism?](#What is a consensus mechanism?)
		* [Proof of Work](#Proof of Work)
		* [Proof of Stake](#Proof of Stake)
		* [Delegated Proof of Stake](#Delegated Proof of Stake)
	* [Using renewable forces other businesses to use fossil fuels](#Using renewable forces other businesses to use fossil fuels)
	* [But other businesses pollute as much](#But other businesses pollute as much)
	* [E-waste](#E-waste)
* [NFTs absurdity](#NFTs absurdity)
	* [NFTs do not prove ownership](#NFTs do not prove ownership)
		* [Note: There are better and less harmful ways of supporting artists](#Note: There are better and less harmful ways of supporting artists)
	* [NFTs and artificial scarcity](#NFTs and artificial scarcity)
	* [What do you really acquire when you buy a NFT?](#What do you really acquire when you buy a NFT?)
	* [What are NFTs useful for?](#What are NFTs useful for?)
* [web3 revolution](#web3 revolution)
	* [The Greater Fool Theory](#The Greater Fool Theory)
	* [The Pyramid scheme](#The Pyramid scheme)
		* [The base of the pyramid](#The base of the pyramid)
	* [The Ponzi scheme](#The Ponzi scheme)
	* [So Pyramid or Ponzi?](#So Pyramid or Ponzi?)
	* [Pump and Dump scheme](#Pump and Dump scheme)
	* [On stablecoins](#On stablecoins)
	* [Tether and propping up Bitcoin](#Tether and propping up Bitcoin)
	* [Are cryptocurrencies usable as currencies?](#Are cryptocurrencies usable as currencies?)
* [It is not the early days](#It is not the early days)
* [Conclusion: Another decentralized web is possible](#Conclusion: Another decentralized web is possible)
* [Further reading](#Further reading)

# Summary

Before giving details and elaborating, I've made a little summary
that explains my arguments, in general terms, on why web3 is harmful and the hype around it. After
this summary, I'll elaborate on important points which will be easier to follow if you know my
overall opinion about web3. 

If I provide a summary is to give a quick image of the topic. **I highly encourage you to read the whole article, the strongest
arguments are near the end** and they can't be understood without reading the previous sections.

## Web3 is harmful

* Cryptocurrencies, NFTs and anything based on **blockchain seriously damage the environment**, since 
blockchains produce huge amounts of carbon emissions. And this
can't be fixed, trying to make them less power consuming would lead to security issues. There's 
nothing that can be done about it.

* **NFTs and cryptocurrencies are a scam**. They have **no real value or utility**. They're only useful as Pyramid/Ponzi/Pump and Dump
schemes and valuable for those ones that make profit from these schemes. For example, it is known that
influencers are
**[taking advantage of the situation](https://fortune.com/2022/01/11/kim-kardashian-floyd-mayweather-crypto-token-ethereummax-lawsuit/)
[and using their popularity](https://www.rollingstone.com/culture/culture-news/kim-kardashian-floyd-mayweather-ethereummax-emax-nft-scheme-1282801/)
[to profit from their followers](https://web3isgoinggreat.com?id=2022-01-04-2)**
who don't really understand neither blockchain nor the economics behind it.
Powerful investors, venture capitals and corporations are making a ton of
money and they're the ones pushing the most web3.

* There's **no real decentralization behind web3 technologies** and even when
there is, it relies on the already established protocols and it just adds another layer of
abstraction. **Current web3 is composed by a few centralized exchanges and marketplaces**.
In the best of the cases, blockchain is a super slow, highly inefficient and a power
hungry alternative to the already existent decentralized solutions.

* [Corporations use their influence, power and market share to boost web3](https://www.theverge.com/2022/1/2/22858698/samsung-2022-tvs-nft-support-announced-cryptocurrency) 
and the underlying cryptocurrencies, following the structure of a Pyramid scheme. This confirms that **the more
money you are able to invest, the more beneficial web3 will be for you**. Rich people are the ones getting
even wealthier thanks to cryptocurrencies.

* web3 isn't on it's early days. This is not a valid excuse for the serious web3's flaws.

## Why is there so much hype if it is a scam?

If cryptocurrencies and NFTs are a scam and if they don't have any real value, then why is there so much
hype with them? Well, I believe there are several reasons:

* **The technical disadvantages of web3 are so serious and obvious that the hype around them is undoubtedly a
technique to push the underlying cryptocurrencies**. It's part of Pyramid/Ponzi/Pump and Dump schemes.

* **The [Greater Fool Theory](https://wikipedia.org/wiki/Greater_fool_theory)**. A "fool" buys an
overpriced item thinking that the item may be sold later for an even higher price to a "greater fool".
> They buy something not because they believe that it is worth the price, but rather because
> they believe that they will be able to sell it to someone else at an even higher price

* Their **criminal and money laundering capabilities**. Even if this shouldn't be a valid argument
when talking about technologies, because things like Tor are also (ab)used for criminal activity, but the key
difference is that Tor has utility far beyond criminal activity: censorship circumvention, the
right to privacy, anonymity on the internet, freedom of press, etc. **Blockchain is great just to
destroy the planet**. It's incredibly inefficient and slow at everything else it does, while other less
harmful solutions exist.

* **[Investors](https://www.thenationalnews.com/business/cryptocurrencies/2021/12/11/asian-venture-capital-companies-and-crypto-funds-to-invest-100m-in-assembly-blockchain/) [are the ones](https://fortune.com/2021/12/01/investors-fund-crypto-startups-us-china/) [pushing](https://www.coindesk.com/business/2021/12/17/reddit-co-founder-creates-200m-fund-with-polygon-for-web-3-social-media/) [the most web3](https://venturebeat.com/2021/12/02/binance-and-animoca-brands-launch-200m-fund-for-crypto-gaming-startups/)**.
It's natural, since they're the ones who will be making money
if web3 (and hence the underlying cryptocurrencies) grows.

* Some people that want a decentralized web are convinced that blockchain is the solution to all the current
problems of web2. "Web3 is the future", they say. They have fallen into the scam. I understand these people though:
I also want and fight for a decentralized and *libre* internet. But **blockchain isn't the solution**.

# Decentralization

The so many times mentioned "feature" and almost slogan of web3, decentralization, is only true in theory.
In practice, it isn't happening. There's very little decentralization, if any, in web3. And even in
the rather improbable case that they achieve a decentralize ecosystem, web3 has serious design flaws
that can't be solved by decentralization. But more on that later.

For end users, there's almost no decentralization at all: 

* If they want to exchange cryptocurrencies, they will most likely be using a centralized platform,
being the most popular ones Coinbase and Binance. So yes, the blockchain (the database) is
decentralized but most of the transactions are made in centralized platforms. Coinbase chief product
officer, Surojit Chatterjee, has stated that **[they want to become the "AWS of crypto"](https://frcusvi.org/coinbase-cloud-wanting-to-be-the-aws-of-crypto-amasses-billion-in-staked-assets/) with Coinbase
Cloud**, effectively centralizing web3.

* For NFTs, at late 2021, **[OpenSea holds more than the 95% of market share](https://qz.com/2073503/coinbase-poses-a-threat-to-openseas-nft-dominance/)**
Web3 advocators will say that it is only a matter of time and that it will be decentralized on the
future. I doubt it. The situation will be similar to the [FAANG oligarchy](https://en.wikipedia.org/wiki/Big_Tech).
**A few marketplaces will raise and together will hold more than the 95% of market share. Just like what happened with web 2.0** and
BigTech. Now OpenSea holds almost the entire market share, then it will be OpenSea and one, two, or three at most other
marketplaces. Just as centralized.

By now, it's the time when our friend, the [crypto bro](https://www.urbandictionary.com/define.php?term=crypto%20bro),
comes and says that decentralized cryptocurrency exchanges exist and they're in use. Yeah, this is true (even if the
market share is insignificant, compared to the popular centralized ones). But these
"decentralized" cryptocurrency exchanges are also, in a lower level, centralized and have the same
problems that web2 has. Remember [AWS](https://en.wikipedia.org/wiki/Amazon_Web_Services) massive outages? Tons of websites
and services went down with it. It is a thing to be expected, since almost a third of the internet
runs on AWS. This is web2. And web3 suffers from the exact same problem. The proof?
[Here](https://www.businessinsider.in/investment/news/aws-outage-shows-that-dexs-arent-are-decentralised-as-expected/articleshow/88186644.cms):
Many of the **popular cryptocurrency exchanges broke due to AWS outages**, notoriously the two big ones:
Binance and Coinbase. But the thing that concerns us is that **[even decentralized exchanges went
down](https://nitter.net/dydxprotocol/status/1468293558360805381) [with AWS outage](https://www.businessinsider.in/investment/news/aws-outage-shows-that-dexs-arent-are-decentralised-as-expected/articleshow/88186644.cms)**. 
The dydx exchange, which allows for peer to peer transactions also went down during AWS outage.

AWS isn't the only centralized service that some web3 services are relying on. Some of the exchanges
and marketplaces are behind Cloudflare's anti-DDoS service,
even though **[what Cloudflare does](https://git.redxen.eu/dCF/deCloudflare/src/branch/master/readme/en.md)
like supporting censorship from oppresing governments and creating a centralized web** seems to be directly opposite to what web3
stands for. 

Other centralized services that some marketplaces and exchanges use and depend on are CDNs
like Fastly or, a second time, Cloudflare. 

And my personal favorite: **several cryptocurrency exchanges are using Google's adsense
and Google analytics** to get ad-revenue. So, **web3 is a revolution against Web 2.0 and BigTech, yet
they depend on Amazon Web Services and Google's adsense and analytics**. I must say that blockchain based ad-networks
exist, but those are much worse than Google's, as they recognise [on this Bitcoin forum](https://bitcointalk.org/index.php?topic=4601067.0)

So, we've seen how web3 is failing at being decentralized and that, in many cases, it experiments the
same problems that Web 2.0 has. Actually, they rely on the same centralized services as Web 2.0

## Offering a Web 2.0 vs a web3 service

To offer a web service, you first need a domain name, so you'll be needing a registrar. To
store your service you'll need to rent a server and storage from a provider of your choice. Then,
the computation when someone uses your service is split between your server ,which runs the HTTP
server, is storing the database and doing whatever backend related computations, if any, and the
JavaScript code
being executed in your client's browser. You are relying on two centralized services, being the
server provider and the domain registrar. You can get a domain at Namecheap for $7.98/yr and a VPS
at Ionos for $7/month with 2v cores, 2 GB of RAM and 80 GB. Later we'll see the costs of running an
web3 service.

Now, let's analyze what web3 adds to this equation. You'll also need a domain name too. And we'll
also need a server. In this case, the server will be the decentralized computer network running as
nodes. So to the client side computations I also need to add a wallet (and in this example we're
assuming that the client isn't using a centralized service as a wallet) to the client side and the
browser. The client would then have to pay the computation fees with the chosen cryptocurrency and the
leftovers go to my personal wallet. 

I'd like to highlight that in a lot of cases developers don't
interact directly with the blockchain, but with an API provided by other service, which is again
centralizing web3. But let's ignore this detail for our example.

Is this any better than the usual setup? Well, let's focus on the super popular Ethereum now, whose
creators invented the concept of web3 and the most used blockchain in web3 "applications".
I want you to read these paragraphs from this 
[excellent article](https://www.usenix.org/publications/loginonline/web3-fraud):

> Estimating the cost (measured in ‘gas’) of an arbitrary computation is complex but let’s assume that we are only interested in the simplest operation: 256 bit integer addition. Each addition costs 3 gas each, so on a worldwide basis this system rates at 600,000 adds per second.

> Compare this amount of compute to a Raspberry Pi 4, a $45 single-board computer which has four processors running at 1.5 GHz.  Each core has 2 ALUs and it will take 4 instructions to perform a 256 bit addition, as the basic unit for the Raspberry Pi (and most other modern computers) is 64 bits.  Each core has a peak performance of 750,000,000 adds per second for a total peak of 3,000,000,000 adds per second.  Put bluntly, the Ethereum “world computer” has roughly 1/5,000 of the compute power of a Raspberry Pi 4!

> This might be acceptable if the compute wasn’t also terrifyingly expensive.  The current transaction fees for 30M in gas consumed is over 1 Ether.  At the current price of roughly $4000 per Ether this means a second of Ethereum’s compute costs $250. [...]

As you can see, **web3 computations are absurdly expensive**, because blockchain is by design an
incredible inefficient technology. At the cost of a second of computations in the Ethereum
blockchain
($250), you could rent a VPS + domain name or buy a bunch of Raspberry Pis or similar [SBC](https://en.wikipedia.org/wiki/Single-board_computer) which
performs far better and that is much cheaper than the "Ethereum world computer". This, without mentioning that
the Raspberry Pi consumes very little energy compared to a usual PC, while the Ethereum blockchain
is a power hungry monstrosity (more on the power that cryptocurrencies consume later).

And if I wanted to only test to build a web3 service then I would have to buy the cryptocurrency
of choice, in this case Ethereum, entering their pyramid scheme and being the ["greater fool"](https://wikipedia.org/wiki/Greater_fool_theory).

# Immutability

The so claimed immutability is a doubled edge sword in web3 as their famous phrase of "code is law"
isn't always respected.

Since blockchains can't be edited, you can't reverse a transaction once you've made it. So any
stolen money can't be given back to their legit owners. This is often considered as a design feature of
blockchains. "Code is Law".

You may like it or not. Personally, I don't. But the worrying thing is that this blockchain
principle isn't being respected.

If blockchain can't be edited, then how can they
reverse a transaction? Well, the team behind the blockchain may create a hard
fork, that in easy words means switching to an older copy of the blockchain before the transaction
that wants to be edited happened.

There's a long history of hard forking blockchains. The most popular
example was what happened with the first DAO: **In 2016 a malicious group managed to exploit a
vulnerability and stole 3.6 ETH (about $50 million in those days). The Ethereum blockchain was
hard forked** and gave back the ETH to the community.

In more recent days, to prove that this isn't something that happened once on the early days and it hasn't
been repeated, **only a few weeks ago the team behind [Polygon hard-fork it](https://tokenist.com/polygon-matic-network-makes-emergency-hard-fork-for-security-risk/)
due to a security vulnerability**.

These hard forks were made to give back stolen money. Those are great news. The dangerous thing about it is that **the
people who decide to hard fork have the capacity of doing it and they may use it as their own
benefit**, which was the reason why blockchain is immutable by design in the first place. 

We can also observe that web3's fake decentralization also help to break immutability: **OpenSea, the
major NFT marketplace [can freeze any NFT](https://www.vice.com/en/article/y3v3ny/all-my-apes-gone-nft-theft-victims-beg-for-centralized-saviors)
from being bought on their platform (which has a 95% of the
market share)**. This is **solely possible because it is a centralize service**. Even if NFTs aren't delisted
on the blockchain, by being frozen on OpenSea they're effectively dead.

# Environmental impact

[web3](https://www.nytimes.com/2021/04/14/climate/coinbase-cryptocurrency-energy.html) [is](https://ccaf.io/cbeci/index) [destroying](https://hbr.org/2021/05/how-much-energy-does-bitcoin-actually-consume) [our](https://www.nytimes.com/interactive/2021/09/03/climate/bitcoin-carbon-footprint-electricity.html) [planet](https://www.nature.com/articles/s41558-018-0321-8).

According to the [Harvard Business Review](https://hbr.org/2021/05/how-much-energy-does-bitcoin-actually-consume)
Bitcoin alone uses 110 Terawatts per year. This is as much as a whole cold country as Sweden. It supposes
the 0.5% of the world's energy usage. And cryptocurrencies energy consumption increases over time,
since the required operations to mine it increase in difficulty.

Not only mining consumes energy, but making transactions too. As [Fortune reports](https://fortune.com/2021/10/26/bitcoin-electricity-consumption-carbon-footprin/)
each Bitcoin transaction uses 1810 kWh. So **[a single Bitcoin transaction](https://www.statista.com/statistics/881541/bitcoin-energy-consumption-transaction-comparison-visa/)
consumes as much as one million Visa transactions**. According to the price of electricity in the
USA, [a kWh of electricity costs 13.90
cents](https://www.eia.gov/electricity/monthly/epm_table_grapher.php?t=epmt_5_6_a), that means that
**each Bitcoin transaction costs $251.54 to make**.

Mining consumes such a ridiculous amount of energy that [miners are buying](https://e360.yale.edu/digest/bitcoin-miners-resurrect-fossil-fuel-power-plant-drawing-backlash-from-environmentalists)
[entire abandoned power plants](https://techstory.in/bitcoin-miners-is-buying-power-plants-to-mine-crypto-now/).


## Is there any solution?

The short answer is stop using them. The long answer will require a little bit of context:

The huge amount of energy consumption is due to blockchain's security measures. The proof of work
consensus method described in, Bitcoin inventor, [Satoshi's paper](https://bitcoin.org/en/bitcoin-paper)
is the most popular one.

Below you can find what's a consensus mechanism and the three main ones. As you'll find out, **if you
don't want to directly give the power to those who has the money, the only viable option is proof of
work, which wastes tons of resources in redundant operations**.

### What is a consensus mechanism?

A consensus mechanism is a method for validating entries into a distributed database and keeping the blockchain secure.

Proof of work is the most secure consensus method at the cost of tremendous wastes of energy. There are other
consensus methods which are also flawed by design. These are Proof of Stake and Delegated Proof of
Stake.

Read technical analysis of consensus mechanisms and their inevitable drawbacks [here](https://scribe.rip/why-nfts-are-bad-the-long-version-2c16dae145e2#48c3)

### Proof of Work

> [Proof of work](https://www.investopedia.com/terms/p/proof-work.asp) is a decentralized consensus mechanism that requires members of a network to expend effort solving an arbitrary mathematical puzzle to prevent anybody from gaming the system.

> Proof of work at scale requires huge amounts of energy, which only increases as more miners join the network

This huge amount of energy is required because the complexity of the mathematical puzzle increases
as the cryptocurrency grows. This has caused that in [2009 you could mine a bitcoin in a few seconds](Today, it takes a warehouse full of specialized computers)
on your PC. Today, it requires a warehouse full of specialized hardware, which consumes obscene amounts of energy.

### Proof of Stake

> With [Proof of stake](https://wikipedia.org/wiki/Proof_of_stake), cryptocurrency owners validate block transactions based on the number of coins a validator stakes.

This has the huge benefit of not needing absurd amount of power as proof of work does, but it has a
major flaw that in my opinion leaves it unusable: **those with enough money can just buy 51% of the
tokens and have total control over the blockchain forever, and gain more and more money and
power over the blockchain**. “He who has the gold makes the rules”.

### Delegated Proof of Stake

> [Delegated proof of stake](https://wikipedia.org/wiki/Proof_of_stake) (DPoS) systems separate the roles of the stake-holders and validators, by allowing stakeholders to delegate the validation role

In practice, there's no real change from proof of stake since as you can imagine if those who have
the money can delegate on who they want, it is fairly simple to abuse it.

## Using renewable forces other businesses to use fossil fuels

Due to the limited capacity of generating renewable energy available both now and for the foreseeable
future, the huge power needed to mine proof of work cryptocurrencies doesn't change
whether they use renewable energy or not. Using renewables to mine forces other businesses
to use fossil fuels by highly increasing price of renewables and severely limiting their availability.

[This article](https://www.business-standard.com/article/markets/nordic-region-bitcoin-s-green-haven-is-running-out-of-surplus-electricity-121041700536_1.html)
shows how cryptocurrencies farm are increasing prices of renewable energy in the Nordic region and
how specifically Bitcoin is eating up the renewable energy in Iceland due to its insane 
energy requirements.

## But other businesses pollute as much

**The argument of things like Youtube, web servers and the gaming industry pollute as much as the
cryptocurrency market is commonly used by cryptobros** whenever you mention the impact that mining
cryptocurrency and its transaction is having on climate change.

**These sectors provide some real value, NFTs and cryptocurrency are worthless outside
speculation**. For example, I personally despise the gaming industry,
but at least it contributes something to society. NFTs and cryptocurrencies only use is speculating with
them and they have no intrinsic value.

**Even if cryptocurrencies were useful for something, they're terribly inefficient. Efficient,
cleaner alternatives exist**. Imagine creating
something similar to Youtube for web3 using blockchain: the resources it'd need would be a
completely madness. **Remember when we saw how a Raspberry Pi is orders of magnitude more powerful than
the "Ethereum world computer"**.  A sane federated and distributed alternative for Youtube already exists and
it's [Peertube](https://joinpeertube.org/#what-is-peertube). Well, a small instance of Peertube runs perfectly
fine on a Raspberry Pi, a mini computer that consumes very little energy. But as they don't push any shady
cryptocurrency business projects like Peertube don't get the attention that web3 receives.

In the end, **blockchain and its inefficient design aren't worth consuming such insane
amounts of power at all**.

## E-waste

**[Yahoo Finance](https://finance.yahoo.com/news/bitcoin-mining-produces-30-700-131735088.html#:~:text=According%20to%20estimates%20by%20researchers,into%20a%20bigger%20environmental%20problem.)
reports that only Bitcoin mining produces around 30,700 tons of e-waste** each year. You can think of
this as **more than an iPhone per transaction**. This is due to the short live of the hardware used to mine
cryptocurrency which, on average, last 1.3 year before they are replaced.

Cycling hardware so fast is necessary, since **Bitcoin's required power to be mined is exponentially
increasing for each Bitcoin mined**. This encourages Bitcoin miners to replace their hardware with
newer one as often as possible. 

As you could probably guess, **the untenable madness of occupying entire power plants and cycling
hardware that fast, has serious consequences**. The situation has been
**one of the reasons behind the global chip
shortage** that we're currently experimenting and that is increasing prices in sectors like
computer hardware, mobile phones, cars, etc.

# NFTs absurdity

![monkee.png](/images/monkee.png)

Monkee.png | #2643 Bored Ape

The NFT of the image above, generated by an [AI](https://en.wikipedia.org/wiki/Artificial_intelligence), **is
[sold](https://opensea.io/assets/0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d/2643) by the price of 86 ETH
that at the time of writing is $289,096.72. Believe it or not, there's people who buy these things**. I easily downloaded
the pic from my browser completely for free and included it in this article just to bother NFT
advocators. 

Pay attention to the previous paragraph. I said the "The NFT of the image above is sold" and not
"The image above is sold". Because **what is being sold is a piece of the blockchain that says that
the image in a certain URL is owned by you**.

I find quite fun the fact that you aren't even buying a copy of the image, since **[the blockchain doesn't contain
the image](https://erickhun.com/posts/nft-misconception-image-arent-on-blockchains/)**. 

This is due to how blockchains work. Including the full image in the blockchain would be
incredibly slow and costly. Including an image on the Ethereum blockchain as the prices
of today would cost more than $100 per kilobyte. The price of storing an image of just 300 kb would be
over $300,000!

So instead of including the image on the
blockchain, **the NFT includes an URL to the image** hosted somewhere. By the way, it is common to
find URLs pointing to regular web servers, so once again web3 is being centralized. To be fair,
there is a solution for this and it's using IPFS but I encourage you to look at the blockchain and see how
many NFTs are being stored on regular web servers. You'd be surprised.

## NFTs do not prove ownership

NFTs are intended to be used as digital contracts that prove ownership. But actually, NFTs don't
prove ownership at all. 

A NFT is a piece in a blockchain with an URL to an item. They don't even need said URL to work. NFTs
could be sold just by giving them a name. Obviously, most of them point to an image or some other kind of
artwork, but **these act just as a hook because buying the NFT you won't actually own the artwork**.

What you will have is a digital contract verifying that you own the NFT. The NFT. Not the artwork.
The artwork can be any media file, obtained from anywhere on the internet. If you do a [quick
search](https://searx.gnu.style/search?q=nft%20twitter%20bots)
you'll discover that there are thousands of [Twitter bots stealing art](https://www.gizmodo.com.au/2021/10/nft-bots-tshirt-online-twitter-war/)
from Twitter artists and sell those
[stolen artworks as NFTs](https://web3isgoinggreat.com?id=2021-12-17-1). The money doesn't go to the artist, who may not ever be aware that their
artwork is being sold as a NFT. **Since the NFT can't prove that the
artist was who listed the NFT in the blockchain. Anyone can create an NFT with any file**. [NFTs don't support
artists](https://thatkimparker.medium.com/most-artists-are-not-making-money-off-nfts-and-here-are-some-graphs-to-prove-it-c65718d4a1b8).
In fact, they allow third parties to profit from stolen artworks.

Oh and wait, **NFTs can't even assure that you legally acquired themselves**. There are thousands of NFTs which have been hijacked, but if you
look to the NFT in the blockchain you will see that the actual owner of the NFT is the hijacker.
[This](https://cryptonews.net/en/news/nft/2957969/) [has](https://web3isgoinggreat.com?id=2021-12-28-1) [happened](https://www.vice.com/en/article/y3v3ny/all-my-apes-gone-nft-theft-victims-beg-for-centralized-saviors) [multiple](https://web3isgoinggreat.com?id=2021-12-23-0) [times](https://web3isgoinggreat.com?id=2021-12-28-0). There's nothing you can do about it but contacting OpenSea (or
any other dominant marketplace in the future) to
prevent the NFT from being bought (the NFT will continue to be on the blockchain but as most transactions are made through
OpenSea selling it will be a difficult task for the thief). Once again, **web3 is [relying on
centralized platforms](https://www.vice.com/en/article/y3v3ny/all-my-apes-gone-nft-theft-victims-beg-for-centralized-saviors)
in an attempt to partially mitigate severe design flaws**.

To conclude, it is clear that **NFTs don't prove ownership**. **They simply register a transaction** from person
A to person B in a blockchain. And they don't even prove that the transaction from A to B is a legitimate one. 
**Illicit transactions can't be reversed**. They can exclusively be addressed by begging the centralized marketplaces
to disable transactions of the stolen NFTs. No NFT can be delisted from the blockchain.

### Note: There are better and less harmful ways of supporting artists

NFTs are flawed by design, but there are ways of supporting your favorite artists that don't need to
run in a blockchain which is a danger to the environment. Platforms like Patreon/Liberapay (warning:
Patreon doesn't respect your privacy) are a better way of supporting an artist and way healthier for
our planet.

Actually, it seems that **NFTs aren't just not helping artists to make a living, but [hurting their
work](https://web3isgoinggreat.com?id=2021-12-17-1)**

## NFTs and artificial scarcity

Quoting Wikipedia, [artificial scarcity](https://en.wikipedia.org/wiki/Artificial_scarcity) is scarcity (insufficiency) of items despite the technology for production or the sufficient capacity for sharing.

Given the capability of computers to create infinite copies of any digital item (be it an image, a
song or a computer program) NFTs are creating artificial scarcity just to profit off. 

Imagine that some food supplier decided to on purpose create a shortage of potatoes just because they want to
sell them at exaggerated prices. Well, the case with NFTs is even an unfairer business, because digital images
can be freely copied and shared limitless, but NFT sellers create an artificial scarcity of these
images so they can make money.

I love the explanation given in this [newsletter](https://www.garbageday.email/p/right-clickers-vs-the-monkey-jpg):

> Digital scarcity is an anti human evolution ideology that imposes board game-like rules which serve no purpose than to preserve the game itself - to hide the internal contradictions of capitalism that become painfully obvious in an area of culture that has overcome scarcity.

## What do you really acquire when you buy a NFT?

Well, I'm glad that you asked this question, because the answer flawlessly reflects the insanity of
the NFT world.

As a summary of the above sections, we can conclude that **when you buy a NFT you're actually buying a
piece on the blockchain which states that you've effectively bought an item stored in a given URL**.
The blockchain can't prove that you, or any of the former buyers own or ever owned, the actual item. It only
proves that there was a transaction between you and the former buyer. The NFT doesn't assure you
that the contents of the URL stored in your NFT won't be changed. We've seen that [it has
happened](https://web3isgoinggreat.com?id=2021-03-09-1).
And it also doesn't prove that other NFT somewhere isn't pointing to the exact same item than yours.
So other NFTs pointing to the same image may exist.

Do you want to hear the best part? Even if your NFT was not stolen, it is never removed from the URL and it was never
duplicated **you still don't own the NFT** of the item. **You do not own a NFT in any legal or
moral sense. NFT ownership isn't recognized by any court in any country nor international
institution**.

## What are NFTs useful for?

Well, if they can't prove ownership over anything and if they can't even prove legal transactions of
the NFT itself (there have been tons of reports of stolen NFTs and someone may be forced to do a
certain transaction through extortion), then **the only use for NFTs is speculating and
pushing the underlying cryptocurrency**.

It is not so difficult to realize that NFTs are used as a merely speculative item. Even if you
were buying the actual item (which doesn't happen, you just pay for a digital contract that happens
to link to an image) most of 
expensive **NFTs don't have any real artistic value, since they are generated automatically by a
computer** which can create thousands of them per second.
[Their price is completely arbitrary](https://cointelegraph.com/news/opensea-collector-pulls-the-rug-on-nfts-to-highlight-arbitrary-value).
Still, they are being sold under unmerciful and exorbitant prices. 

See the [monkee.png](/images/monkee.png) at the beginning of this section as an
example. It is a monkey picture generated by an AI which [is being sold](https://opensea.io/assets/0xbc4ca0eda7647a8ab7c2061c2e118a18a936f13d/2643)
at the price of $289,096.72.
In the same [collection](https://opensea.io/collection/boredapeyachtclub) of NFTs we can find thousands of almost identical monkey pictures selling at
similar prices. Since the NFTs doesn't have any real value as a piece of art, their price is given
exclusively by the [greater fool theory](https://wikipedia.org/wiki/Greater_fool_theory). **NFTs are
only useful for speculation and easily reselling stolen artworks**.

In order to upset cryptobros, you can **[GET FREE MONKEE PICS HERE](/pages/free-apes.html)**.

# web3 revolution

"Web3 is the future". "A web based on blockchain: the world-changing technology". "Web3 will
democratize the internet". "Cryptocurrencies can save people on third world countries from inflation". "Web3 empowers the people, not BigTech". "Blockchain is revolutionary".
"Blockchain is the greatest innovation of the XXI century".

As the Nobel laureate economist Robert Shiller said when asked about Bitcoin: **"It’s such a wonderful story. If only it were true"**

But according to cryptocurrency enthusiasts, the web3 revolution is coming and it will bring us a
decentralized internet based on blockchain.

And if you don't believe in web3, it can only be for one of these reasons:

1. You're too old
2. You're too stupid
3. You're the enemy of the revolution

Yeah, I must be either too stupid or too old because I don't believe in the new world that web3 will
bring us, where we will all live in our luxurious mansions bought using cryptocurrencies, and of course
located in the best district of the [metaverse](https://en.wikipedia.org/wiki/Metaverse). Dressing
ourselves and decorating our houses with NFTs definitely sounds ideal and revolutionary.
Who hasn't always wanted a virtual mansion decorated with our favorite [monkee
pngs](/images/free-apes.html)?
A world where our virtual houses will consume far more power than our physical ones. Doesn't it
sound as your dreamt world?

Now seriously, **we should, at the very least, remain skeptical** of a revolution based on a technology which in an
almost overwhelming majority of the cases is used for speculation, a new web that promises to be decentralized
and immutable but then centralized marketplaces have full power to freeze transactions and make
their tokens effectively worthless, a technology that is killing the planet, **a technology that just shines
in its lack of efficiency**. Ultimately, a revolution that want us to believe that the insane crypto-casino
where prices are only dictated by how much a buyer is willing to pay and not any physical or real
value is a good idea and will reduce the differences between social classes.

Whenever I read "web3 revolution" I can't avoid thinking about the wise words of George Orwell in
his masterpiece, Nineteen Eighty-Four:

> One does not establish a dictatorship in order to safeguard a revolution; one makes the revolution in order to establish the dictatorship

Blockchain may be revolutionary. It also is innovative.
But neither all innovations nor all revolutions are good. Actually,
there are some innovations which are just awful: see [CFCs](https://wikipedia.org/wiki/Chlorofluorocarbon?lang=en)
or building with [asbestos](https://wikipedia.org/wiki/Asbestos). Blockchain belongs to this type of innovation.

Transitioning from Web 2.0 to web3 would bring little decentralization, if any, to the web. You'll
be replacing current BigTech corporations (Google, Amazon, Microsoft, Apple and Facebook) with the millionaire investors,
Venture Capitals and centralized platforms (Coinbase, Binance, OpenSea) that are being built on web3. Either way, the average
person will be under the control of a rich minority. It perfectly fits with Orwell's words, web3 is a revolution to replace the current
BigTech dictatorship with a new one.

web3 isn't going to bring any "social justice" at all. All of those nice slogans are pure
propaganda. In fact, web3 seems to be pretty much the opposite to what they claim: In **the NFT
market, 80% is owned by the 9% of accounts. And the 95% of Bitcoins are owned by a 2%**.

I'd like to quote [Stephen Diehl](https://www.stephendiehl.com/blog.html)'s words, who described
web3 and cryptocurrencies like this:

> A giant regressive tax that transfers money from the poor and illiterate, to the early adopters and the investors and the technologists.

**"The web3 revolution" seems to be a brilliantly orchestrated marketing campaign for a huge fraud**.

In the next sections we'll see how many similarities arise when comparing the cryptocurrency and
NFT market with
different infamous scams: The Ponzi scheme, the Pyramid scheme and the Pump and Dump scheme.

## The Greater Fool Theory

Before explaining the schemes, it's important to know what is [the greater fool](https://wikipedia.org/wiki/Greater_fool_theory) theory since these
scams are based on it.

> The greater fool theory suggests that one can sometimes make money through the purchase of overvalued assets — items with a purchase price drastically exceeding the intrinsic value — if those assets can later be resold at an even higher price.

> In this context, one "fool" might pay for an overpriced asset, on the assumption that he can probably sell it to an even "greater fool" and make a profit. This only works as long as there are new "greater fools" willing to pay higher and higher prices for the asset. Eventually, investors can no longer deny that the price is out of touch with reality, at which point a sell-off can cause the price to drop significantly until it is closer to its fair value

> Due to biases in human behavior, some people are drawn to assets whose price they see increasing, however irrational it might be. This effect is often further exacerbated by [herd mentality](https://wikipedia.org/wiki/Herd_mentality?lang=en), whereby people hear stories of others who bought in early and made big profits, causing those who did not buy to feel a fear of [missing out](https://wikipedia.org/wiki/Fear_of_missing_out?lang=en).

The assets in our case are cryptocurrencies and NFTs. And quoting [the economics Professor John Quiggin](https://nationalinterest.org/commentary/the-bitcoin-bubble-bad-hypothesis-8353)

> Bitcoins will attain their true value of zero sooner or later, but it is impossible to say when.

## The Pyramid scheme

**The cryptocurrency market is an asset bubble sustained exclusively by the existence of greater fools
and manipulated, unregulated markets**.

That's why Silicon Valley cheerleaders and cryptobros are pushing cryptocurrencies and web3 so hard on Twitter
and pretty much everywhere, just because they'll greatly profit from a major adoption of these and
the only sustenance of the market are greater fools joining it.

Now look at the definition of a [Pyramid scheme](https://constantinecannon.com/practice/whistleblower/whistleblower-types/financial-investment-fraud/ponzi-schemes/):

> A pyramid scheme, also called a chain referral scheme, is a fraudulent business model in which new members are recruited with promises of payment tied to their ability to enroll future members in the scheme. As the membership pool expands exponentially, further recruiting becomes impossible and the “business” becomes unsustainable.

This is what web3 is really about. It's a pyramid scheme where the only way early
adopters make money is if more people fall in the scheme. But web3 seems to be even better than a
classical Pyramid scheme in the sense that members don't really know what blockchain is about, they
just know that they can make money and, unlike the classic frauds, there won't be a central authority to blame when
everything collapses.

The term web3 was [created by the Ethereum co-founder Gavin Wood](https://www.wikiwand.com/en/Web3).
I must recognize that the plan was brilliant. The creation of an ecosystem on top of their blockchain is a
master strategy to boost the adoption of the underlying cryptocurrency. If web3 was widely adopted,
they would keep having greater fools for a long time. A great way of making
himself, as an early adopter, investor, and co-founder of Ethereum, filthy rich.

### The base of the pyramid

One extremely dangerous thing about cryptocurrencies is the community built around them. This
community is everywhere on the internet: Reddit, Twitter, forums and even cryptocurrencies exchanges like
RobinHood are trying to create a sense of community.

These communities are largely integrated by kids. Young people that feel excited about
web3 because it's something new and they see it as revolutionary. Generally speaking, these kids are mostly interested
on the technology under it while seniors seem to be more interested on the economic part.

The cryptobro community is key in the success of web3. People attracted to cryptocurrencies because
they've read some great stories about early investors getting rich or they've read a tweet about
investing in cryptocurrencies and then they don't leave because they easily make friends on these forums, where
people share memes about cryptocurrencies, shitposting and creating polls about coins and NFTs where
they share their thoughts on which coin will raise in
price next month, where people tell their success stories speculating, etc. They help each other to join
the web3 world (the pyramid scheme).

You can see it yourself if you want. Go to Reddit and search for any cryptocurrency related subreddit.
Read posts. They're kissing each others asses all the time. Disagreements are simply caused because they
have different opinions about some coin. Some cryptobros worship Bitcoin, others worship Ethereum. There are
cryptobros who feel identified with a
certain cryptocurrency and its community - be it Bitcoin, Ethereum, Litecoin or whatever - and will defend it to the
end, like some kind of patriotism. Just like what happens with the [Operating Systems Wars](https://wikipedia.org/wiki/Operating_system_advocacy). 
Or the [editor wars](https://wikipedia.org/wiki/Editor_war?lang=en). But with a giant pyramid scheme
behind.

**Cryptobros act as the recruiters would in a traditional pyramid scheme**, most of them without even noticing.
There are also cryptobros that actually know that bringing people into
cryptocurrencies will benefit them as the previous greater fools and spam all over Twitter,
Instagram or whatever how cryptocurrencies are great. **Unconsciously or not, they bring greater fools
and act as fantastic hooks**.

## The Ponzi scheme

Others consider cryptocurrencies more similar to a Ponzi scheme. Let's have a look at the definition:

> A Ponzi scheme is a form of fraud that lures investors and pays profits to earlier investors with funds from more recent investors. The scheme leads victims to believe that profits are coming from legitimate business activity (e.g., product sales or successful investments), and they remain unaware that other investors are the source of funds. A Ponzi scheme can maintain the illusion of a sustainable business as long as new investors contribute new funds, and as long as most of the investors do not demand full repayment and still believe in the non-existent assets they are purported to own. 

A [guest post](https://www.ft.com/content/83a14261-598d-4601-87fc-5dde528b33d0) 
in the Financial Times by the Boston University senior fellow Robert McCauley makes a
comparison between the cryptocurrency market and the Ponzi scheme. I'd like to extract some
interesting parts of McCauley's article. He finds these similarities between cryptocurrencies and
Ponzi:

* Investors buy into the plan with the expectation of making a profit.
* Initially, the project meets expectations by delivering profits to investors who cash out.
* The plan’s management withdraws a large percentage of the profits. The program lacks any mechanism to grow profits beyond new investments.
* The project’s originators scoop up a large portion of the money.

**McCauley goes as far as saying that comparing cryptocurrency with Ponzi schemes, gives Ponzi a bad
rap**.

Why does he claim that? Because when the bubble of cryptocurrencies finally crashes, there won't be any central
authority to blame, there won't be a [Bernie Madoff](https://wikipedia.org/wiki/Madoff_investment_scandal)
who can be arrested. **It will be impossible to give back money to the scammed people** without any central
authority to blame for the collapse. **Cryptocurrency founders, venture capitals an early investors will have made a ton
of money by then, but the remaining and most of the later coinholders will lose everything** and there won't
be any legal option that can help them.

The author of "The Politics of Ponzi Schemes", Marie Springer, stated the following when asked about
cryptocurrencies in an interview: **"I can't tell you how many Ponzi schemes have done exactly what you're describing, using exactly the mechanisms you're referring to. The only difference is usually these schemes are done with fiat currency"**

If you want more evidence that the ones making money with web3 are rich investors and venture
capitals, here you have some links which show that they're investing millions in advertising web3
because they desperately need greater fools.

* https://www.forbes.com/sites/jonathanponciano/2021/12/09/bitcoins-biggest-corporate-investor-has-spent-nearly-500-million-buying-bitcoin-as-crypto-markets-lost-700-billion-in-value/
* https://www.coindesk.com/business/2021/12/17/reddit-co-founder-creates-200m-fund-with-polygon-for-web-3-social-media/
* https://www.bloomberg.com/news/articles/2021-11-29/citigroup-alum-matt-zhang-launches-1-5-billion-crypto-venture
* https://venturebeat.com/2021/12/02/binance-and-animoca-brands-launch-200m-fund-for-crypto-gaming-startups/
* https://nftevening.com/venture-capital-firm-andreessen-horowitz-invests-in-nft-platform-pleasrdao/
* https://fortune.com/2021/12/01/investors-fund-crypto-startups-us-china/
* https://finance.yahoo.com/news/microsoft-leads-27-mln-early-160002449.html
* https://blockchain.news/news/huobi-launches-100-million-venture-unit-nfts-bolt-deals
* https://www.forbes.com/sites/elizahaverstock/2022/01/05/nft-billionaires-opensea-founders-each-worth-billions-following-latest-funding-round/

## So Pyramid or Ponzi?

I don't know. Both schemes are really similar to each other. [You may read the few
differences between them](https://constantinecannon.com/practice/whistleblower/whistleblower-types/financial-investment-fraud/ponzi-schemes/). 

In my opinion, the exact
scheme used doesn't really matter. The problem is that it seems to be a fraud of gigantic proportions which
will cause the average investor to lose all their invested money.

## Pump and Dump scheme

> "Pump and dump" (P&D) is a form of securities fraud that involves artificially inflating the price of an owned stock through false and misleading positive statements, in order to sell the cheaply purchased stock at a higher price. Once the operators of the scheme "dump" (sell) their overvalued shares, the price falls and investors lose their money. This is most common with small cap cryptocurrencies and very small corporations/companies.

The Pump and Dump scheme, or as it is known in the web3 argot, rug pull, as [Wikipedia says](https://wikipedia.org/wiki/Pump_and_dump)
is often seen with small cryptocurrencies and NFTs collections.

How it works is really simple: **Founders pump up the value of their digital tokens, drive up the
price and dump (sell) when they can. Most investors are then left with worthless tokens**.

A particular good example is how Kim Kardashian and Floyd Mayweather, people that somehow have
millions of followers, [are being sued for](https://nypost.com/2022/01/12/kim-kardashian-floyd-mayweather-sued-over-alleged-crypto-scheme/)
pushing a cryptocurrency pump and dump scheme. **They promoted the EthereumMAX cryptocurrency on their social
networks (Kim has 250 million followers on Instagram)**. They promoted the cryptocurrency for a
few weeks, Floyd even made it the "exclusive cryptocurrency" to buy online tickets for one of his
boxing matches. **After a while the value of EthereumMax tokens plummeted 98% from their peak**,
causing Kim's and Floyd's followers who bought EthereumMax lose their invested money.

This perfectly reflects the archetype of a pump and dump scheme: You create a worthless digital
token, then you raise the price following the greater fool theory which is possible because two
influencers with millions of followers advertised your product. The average person who follows Kim
or Floyd isn't the one that knows how cryptocurrencies work, so they took advantage of that. And
then, you sell all your tokens making a huge profit and leaving everyone who invested with worthless
tokens that nobody will ever buy.

This isn't an isolated case, but rather one example of a fairly common scam in the web3:

* Squid Game coin scam makes off with $3 million https://www.wired.com/story/squid-game-coin-crypto-scam/
* MetaDAO rugpulls more than $3.2 million https://cryptobriefing.com/metadao-makes-off-with-3-2m-in-rug-pull/
* $60 million disappear in AnubisDAO project https://cryptoslate.com/police-forces-jump-into-anubisdao-saga-after-60-million-rug/
* "Evolved Apes" NFT project  rugpulls $2.7 million https://www.vice.com/en/article/y3dyem/investors-spent-millions-on-evolved-apes-nfts-then-they-got-scammed

There are many more pump and dump web3 stories. This is [a fantastic site to find them](https://web3isgoinggreat.com/), just go to the filters and select rug pull.

## On stablecoins

Stablecoins claim to be less volatile because they're backed one-by-one by an existing asset:
normally a FIAT currency like the US dollar, although it could be gold or other physical item.

If, in my opinion, traditional cryptocurrencies are some kind of mixed Pyramid-Ponzi
scheme, stablecoins seem to perfectly reassemble the structure of Ponzi schemes.

You may read [this Forbes article](https://www.forbes.com/sites/jasonbloomberg/2018/11/25/bernie-madoff-move-over-stablecoins-have-you-beat/)
which gives more detail about the three types of stablecoins and how the three of them are considered a Ponzi scheme.
For the purposes of this article, I'll explain the well-known example of the biggest stablecoin, Tether.

The USDT (U.S. Dollar Tether) is pegged to the dollar one for one and is the third biggest
cryptocurrency by market share, just after Bitcoin and Ethereum. But **there are [serious doubts](https://www.bloomberg.com/news/features/2021-10-07/crypto-mystery-where-s-the-69-billion-backing-the-stablecoin-tether)
that the Tether company has the [$80 billions](https://coinmarketcap.com/currencies/tether/)
that it would need to back their cryptocurrency**. If Tether actually had reserves to back this up, as they claim, they would be one of the largest banks of the world. But it is believed that they don't have such a quantity. The data given to prove
it is insufficient and they don't allow any independent audit to be done.

As **the [Financial Times reported](https://www.ft.com/content/529eb4e6-796a-4e81-8064-5967bbe3b4d9),
in March 2021 "the stablecoin that used to say it was 100 per cent backed by cash reserves is in fact 2.9 per cent backed by cash reserves"**.
The current situation is probably worse, since they don't stop adding billions.

Tether has been fined by the Commodity Futures Trading Commission. [Tether to Pay $41](https://www.cftc.gov/PressRoom/PressReleases/8450-21)
million over claims that Tether stablecoin was fully backed by US Dollars.

Without proving that they actually have backed their tokens, they keep adding more and more tokens. On January 1,
Tether added another [$1 billion](https://nitter.net/usdcoinprinter/status/1477070843574378509).
**They have been accused by various institutions and legislators of "printing money" in what seems to
be a huge Ponzi scheme**.

## Tether and propping up Bitcoin

If you read Forbes' article about stablecoins, then near the end you may have read something really
frightening: **Tether seems to be being used for propping up Bitcoin value**:

The University of Texas finance professor John Griffin, who has more than 10 years report of
spotting financial fraud, and his graduate student Amin Shams published **a [study](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3195066) 
in 2018 titled "Is Bitcoin Really Un-Tethered?". The study says
that at least half of the jump in Bitcoin during 2017 was due to coordinate price manipulation**.

Bitcoin price rose to almost $20,000 in late 2017 after starting the year below $1,000.

In the study, professor Griffin uses the blockchain, in which is recorded every single transaction,
to find evidence of manipulation in the market. Griffin tracked Bitfinex transactions (a company
associated with Tether, sharing most of their executives) and **found that tether was used to buy
Bitcoin after large price falls and they result in large increases of Bitcoin price**. Griffin and his student state that
“These patterns cannot be explained by investor demand proxies but are most consistent with the supply-based hypothesis
where Tether is used to provide price support and manipulate cryptocurrency prices.”

"It was creating price support for bitcoin, and over the period that we examined, had huge price effects", reports Griffin.
"Our research would indicate that there are sophisticated people harnessing investor interest for their benefit."

It was found in the study that **about 87 hours of heavy Tether trading could explain 50% of the boost Bitcoin** experimented,
and around a 64% of the grow of other major cryptocurrencies.

A well-known economist, Nouriel Roubini, reported on [Twitter](https://nitter.net/Nouriel/status/956477828974784512): 
"Tether printing press in high gear, issuing $400 [million] of fake USD in 4 days to manipulate and prop up the price of Bitcoin"

If you need even more evidence, **Tether is also accused of illegally manipulating Bitcoin value by [the US Justice department](https://www.cnbc.com/2018/11/20/regulators-investigate-whether-bitcoin-price-was-propped-up-illegally.html)**.

[Some people speculate](https://web3isgoinggreat.com?id=2022-01-01-1) that the recent addition of $1 billion to Tether's
supply was made in order to pump Bitcoin value which greatly declined over December. **Given the study
and accusations from the US Justice department,
it seems that Tether was used to manipulate Bitcoin price over 2017, and it wouldn't be surprising
that they keep doing it**.

## Are cryptocurrencies usable as currencies?

The words of the Nobel Prize in economics Angus Deaton when asked about Bitcoin were: "Does the world really need another speculative asset?".
And that's the only use case for cryptocurrencies, being speculative assets (at the likely risk of being
scammed). As actual money, they suck.

They've been telling us that cryptocurrencies will help millions of people against corrupt
governments and inflation. But real life is far from that marketing lies that investors and
coinholders want us to believe. As we've seen, Tether and other stablecoins are being used to prop up Bitcoin
so the day Tether can't sustain the fraud anymore (and they have already been sued) it's likely that
Bitcoin value crumples.

A big reason (even if it's sold as a feature, for widely adoption of cryptocurrencies is
definitely a drawback) is that you can't reverse operations. We've seen how this is something
negative for a currency in the forms of irreversible stolen money, typing errors that lead to send
the wrong quantity, phishing attacks that can't be reversed, etc. Scams would arise everywhere.
Actually, we've already seen how the cryptocurrency world is full of scams.

Remember when I told you the costs of a single transaction in Bitcoin? It was $251.54 for a single
transaction. Wouldn't it be truly stupid going to buy groceries and that the cost of the
transaction was 4 times the cost of your shopping cart?

Another important flaw is that cryptocurrencies are too volatile, the market is unstable and
manipulable by investors. Something that moves 5% a day, 20% in a month — up or down — can't be a currency. 
As a consequence, very few sites accept payments with cryptocurrencies, making them virtually useless as
a currency at the present time.

"But after web3 revolution you'll be able to pay with cryptocurrencies everywhere". Yeah, even if such a web3
exists someday, that would be a serious problem.
**Cryptocurrencies shouldn't be used everywhere because they consume huge amounts of energy**. This
problem can't be solved, due to how blockchains work. Proof of work blockchains require wasting
energy. Proof of stake ones can be easily and completely controlled by rich investors, once again
giving the power to central entities like banks or governments.

A good summary is given by Cecilia Skingsley, Deputy Governor at the Swedish Central Bank. 
She believes that Bitcoin doesn’t even count as a currency:

> [Cryptocurrencies] don’t meet criteria to be called money: they don’t store value, they fluctuate, and they’re not at a stable rate of exchange

Something as volatile, unstable and energy consuming shouldn't be used as a currency. It doesn't
even provide an advantage over traditional currencies, since the blockchain is decentralized but
the platforms where the most of transactions happen are fully centralized: Coinbase, Binance,
Opensea, etc. And most of the few sites that will accept cryptocurrencies are using a centralized payment
processor like BitPay. All these companies have to comply with the law. They have 
identifiable data of every user. And they are exposed to any government regulations. **At the end,
there's no real unruled, revolutionary currency. Just an asset to speculate and to - potentially - be
scammed with**. 

# It is not the early days

"It's still the early days" is one of the most common arguments against articles that shows the sad
reality about web3. But it's not the early days. Bitcoin was launched on 2009. Ethereum on 2015.
Stablecoins were invented in 2014. The first DAO in 2016. If blockchain was truly a revolutionary
technology as they advertise it, it would have changed the world by now. Think about it. In 2009 we
were still using phones with physical keyboards. Android and iOS were world-changing technologies.
Blockchain was not.

I don't want to elaborate since this article is long enough and there's [a great article](https://blog.mollywhite.net/its-not-still-the-early-days/)
about this topic by Molly White, who is also the person behind [web3isgoinggreat.com](https://web3isgoinggreat.com/). I particularly
like the last paragraph of her article:

> The more you think about it, the more “it’s early days!” begins to sound like the desperate protestations of people with too much money sunk into a pyramid scheme, hoping they can bag a few more suckers and get out with their cash before the whole thing comes crashing down

# Conclusion: Another decentralized web is possible

Before ending this article, I'd like to add a phrase inspired by a comment I read somewhere (I can't remember where, if you find the source I'll be glad to give attribution)

> We should treat cryptocurrencies like we treat [hard drugs](https://simple.wikipedia.org/wiki/Hard_and_soft_drugs): People want them, suppliers are getting insanely rich off it, and it does no good at all.

I think that web3 is the wrong path, but I completely agree with the idea that we need to build a better web.
**We must keep fighting for a decentralized, private and libre web that empowers the
people and not corporations**. A decentralized and free as in freedom web is possible.

**Using a technology as inefficient and resource wasting as blockchain makes no sense when we
already have
excellent alternatives**. Alternatives that are orders of magnitude less polluting than
blockchain. 

These technologies that can bring us a better internet
do exist but haven't been widely adopted yet
because they haven't been pushed by billionaire investors, venture capitals, corporations nor scammers.

Some examples of great (really) decentralized technology are:

* [i2p](https://en.wikipedia.org/wiki/I2P)
* [The fediverse](https://en.wikipedia.org/wiki/Fediverse) ([Mastodon](https://joinmastodon.org/))
* [BitTorrent](https://en.wikipedia.org/wiki/BitTorrent)
* [XMPP](https://xmpp.org/)
* [GNUnet](https://www.gnunet.org/en/index.html)
* [Peertube](http://joinpeertube.org/)
* [Neocities](https://neocities.org/)
* [Freenet](https://freenetproject.org/)
* [Jami](https://jami.net/)
* [Syncthing](https://syncthing.net/)
* [eDonkey](https://en.wikipedia.org/wiki/EDonkey_network)
* [Yacy](https://yacy.net/)
* [Gitea](https://docs.gitea.io/en-us/)
* [Guifi.net](https://en.wikipedia.org/wiki/Guifi.net)
* [OpenNIC](https://www.opennic.org/)

# Further reading

* https://www.stephendiehl.com/blog.html (great blog about blockchain)
* https://scribe.rip/why-nfts-are-bad-the-long-version-2c16dae145e2 (excellent technical analysis)
* https://www.usenix.org/publications/loginonline/web3-fraud
* https://nationalinterest.org/commentary/the-bitcoin-bubble-bad-hypothesis-8353 (from 2013 and still useful)
* https://yesterweb.org/no-to-web3/
* https://www.stephendiehl.com/blog/web3-bullshit.html
* https://blog.mollywhite.net/blockchains-are-not-what-they-say/
* https://kill-9.xyz/harmful/software/blockchain
* https://greaterfoolcrypto.com/2022/01/01/is-crypto-a-ponzi-scheme-or-worse-than-a-ponzi-scheme/
* https://invisibleup.com/articles/38/
* https://web3isgoinggreat.com/
* https://nitter.net/CoinersTakingLs (memes about web3)
