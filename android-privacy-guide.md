tags: Android Privacy Degoogle
title: Android Privacy Guide: Zero Trust
author: Werwolf
date: 2021-07-28
aside: A large collection of techniques for maintaining your privacy while using an Android phone

I'm willing to get rid off my phone as soon as I am able to. But nowadays it is almost necessary. So I've been learning how to remain private while using one of these "smartphones", although I like to use a term that defines them better, tracking devices.

Even though owning a smartphone and having some kind of privacy seem incompatible, I've been trying my best to reach the maximum level of privacy while using an Android device. 

If you'd like a guide for iOS I can write a really quick one for you right now: **[You can't be private on an Apple device](https://gist.github.com/iosecure/357e724811fe04167332ef54e736670d)**. Their privacy is pure marketing.

In this guide, **we'll gain our privacy in a rational way, based on facts and not on blind trust**. How we will do this? Using exclusively [free software](http://www.gnu.org/philosophy/free-sw.html). It's the only verifiable way of being private. People (Apple fans) who prefer to blind trust a corp can go fuck themselves. 

Forgive my rudeness but this is [the reality, don't believe Apple's lies](https://sneak.berlin/20201112/your-computer-isnt-yours/). Free software is made with user freedom in mind and it can be audited. Corporations' **proprietary software is made with profit in mind. And they'll profit your data**, even if their marketing tells you otherwise. You can't verify what proprietary software does. So we should use exclusively free software, since it gives us privacy and freedom. In this guide we'll follow this principle.

Privacy is an in-depth topic so even if this guide seems too long, I promise you that it will help you with your concerns with Android privacy. Let's start.

## Table of Contents

* [Finding a ROM](#Finding a ROM)
	* [Rooting](#Rooting)
	* [Locked bootloader? Don't want to flash a ROM?](#Locked bootloader? Don&#8217;t want to flash a ROM?)
* [Installing software](#Installing software)
	* [Isolating proprietary software](#Isolating proprietary software)
* [General software recommendations](#General software recommendations)
	* [Alternatives to Google's apps](#Alternatives to Google&#8217;s apps)
	* [More recommended apps](#More recommended apps)
* [Privacy issues and possible solutions](#Privacy issues and possible solutions)
* [Ad and tracker blockers](#Ad and tracker blockers)
* [Changing your DNS server](#Changing your DNS server)
* [Firewalls](#Firewalls)
* [VPN](#VPN)
* [Browser](#Browser)
	* [Search engine](#Search engine)
* [Microphone and camera](#Microphone and camera)
	* [Apps that alert you when the mic is in use](#Apps that alert you when the cam/mic is in use)
	* [Block ultrasonic cross-device trackers and Google Assistant](#Block ultrasonic cross-device trackers and Google Assistant)
* [Erase image metadata](#Erase image metadata)
* [Bluetooth mitigation](#Bluetooth mitigation)
* [Prevent apps from auto-starting on boot](#Prevent apps from auto-starting on boot)
* [Freezing apps running in background](#Freezing apps running in background)
* [Changing the default keyboard](#Changing the default keyboard)
* [Restricting access to your clipboard](#Restricting access to your clipboard)
* [ADVANCED: Limiting apps permissions in depth](#ADVANCED: Limiting apps permissions in depth)
	* [The definitive way of sandboxing proprietary software](#The definitive way of sandboxing proprietary software)
* [Faraday bag](#Faraday bag)
* [Conclusion](#Conclusion)

## Finding a ROM

*Disclaimer: You don't need to flash a custom ROM to follow the rest of this guide, but it is a highly recommended step since Google services and manufacturer's spyware are highly invasive surveillance garbage that can't be removed. If you can't flash a custom ROM, try the [Universal Android Debloater](#Locked bootloader? Don&#8217;t want to flash a ROM?). Anyway, read the rest of the guide since there are useful privacy measures even on Googled phones*

The first step in our journey should be finding and flashing a new ROM. The pre-installed OS by the manufacturer is in almost every case full of spyware and proprietary malware like GAPPS (Google Apps), manufacturer own apps and telemetry services, proprietary firmware, etc.

**The ideal solution would be using [Replicant](https://www.replicant.us/)** since it's the only fully free (as in freedom) OS for smartphones. Check their supported devices list, just in case. The problem is that it's limited to a few specific devices. So if you don't find your device there, don't worry.

There are other custom ROMs that are focused on respecting user freedom and privacy. The ROMs I'd recommend are [LineageOS](https://lineageos.org/), [/e/](https://e.foundation/) and [GrapheneOS](https://grapheneos.org/). Check if your current device is supported. 

If it isn't, you may try to find other ROMs for your device. Make sure to check that the source is trustable. Important requirements are: **focus on being free software and no GAPPS pre-installed**.

I'd recommend going to XDA developers and looking for your phone specific model. You'll find useful guides, ROM devs, user reviews, etc. 

These ROMs (LineageOS, /e/, GrapheneOS, etc) have the con of not being fully free. They require running proprietary firmware provided by the manufacturer. So **the ideal option is Replicant**, the only 100% free OS for mobile phones.

Once you've find a ROM for your device, you'll have to flash it. This process seems to be difficult and complex if it's your first time, but I can promise you that it isn't that difficult. I won't elaborate on this topic since there are thousands of guides online. 

### Rooting

Rooting an Android device is considered an insecure practice. And it is, partially. Rooting in itself isn't insecure, since it only gives you privileges, like the root user on GNU/Linux. The insecurity is that if a malicious attacker gets access to your device, root access might be dangerous to have.

Rooting is a really easy step if you're already flashing a custom ROM. There are tons of guides, I won't elaborate.

My favorite advantage is that I can use system wide adblockers that use the hosts file, like [Adaway](https://adaway.org/). This is much more efficient than VPN based adblockers, eg [Blokada](https://blokada.org/). 

Other great app I use is [AFWall+](https://f-droid.org/en/packages/dev.ukanth.ufirewall/) which is a powerful firewall for Android. It uses directly the Linux kernel, so it needs root privileges. 

**You won't be able to do the cool stuff on the [Advanced section](#ADVANCED: Limiting apps permissions in depth) without root**. Take it into account.

This guide has **some parts that require root access**. You may skip them if you decide to keep you phone unrooted. **I'll always try to give a non-root alternative**.

### Locked bootloader? Don't want to flash a ROM?

Don't worry. I have a little workaround for you. You may use the **[Universal Android Debloater](https://gitlab.com/W1nst0n/universal-android-debloater) tool** which **works on every device**, rooted or not. The README provides instructions. This script is safe to use, it can't brick your phone, especially if you don't use the root mode. So don't be afraid and **improve your privacy now!**

## Installing software

You don't have Google services anymore, so you can't download apps from the Play Store like before. This is fantastic, since **Google services constantly spy on you**. Fuck off Google.

Instead of Google's surveillance system, **we're going to use [F-Droid](https://f-droid.org/)** which is a repository of free software for Android. You can download the client from their official site. Install the apk and start exploring their catalogue. Every app on F-Droid is free as in freedom. This makes F-Droid **the most secure and privacy friendly app market**.

If you ever need proprietary apps for whatever reason (avoid nonfree software at all costs if you value your privacy and/or freedom) the best way to get them is using [Aurora Store](https://f-droid.org/en/packages/com.aurora.store/) which can download apps from the Play Store "anonymously". But don't install proprietary software yet, I'll show how to do it in a way that maximizes your privacy on the next section.

Before installing any proprietary software, try to find an alternative on F-Droid. On the [software recommendations](#General software recommendations) section I've listed some useful apps for everyday use.

### Isolating proprietary software

Is it possible to use proprietary software without compromising our privacy? I don't think so. But sometimes we are forced to use proprietary garbage.

We'll **isolate proprietary software using [Insular](https://f-droid.org/en/packages/com.oasisfeng.island.fdroid/)** (or [Shelter](https://f-droid.org/en/packages/net.typeblog.shelter/) but it has slower development).

You just have to create a new profile using Insular and install the Aurora Store on it. I also recommend blacklisting apps on your [firewall](#Firewalls) and routing the traffic through Tor using [Orbot](#VPN).

And again, please **only use proprietary software when it is strictly necessary**. For most apps you **can find a libre replacement on F-Droid** that will respect your freedom and your privacy. 

You should consider taking a look at the Advanced section since it'll help to restrict permissions and block some known tracking techniques present on non-free apps.

## General software recommendations

In this section **I'll list general apps for the everyday use** of your phone. **If you want to discover great apps that will help you maintain your privacy, here you have my personal recommendations**. Every app listed here is [free software](http://www.gnu.org/philosophy/free-sw.html) and I think that almost all of them are available on F-Droid.

### Alternatives to Google's apps

Since people are used to use Google's apps, I thought that listing some alternatives will help in our degoogling journey. 

- **Google Search**: This topic is covered on the [browsers section](#Browser). But [Searx](https://searx.me/) is considered the best option.
- **YouTube**: [Newpipe](https://f-droid.org/en/packages/org.schabi.newpipe/) is a great privacy friendly and feature rich alternative.
- **Chrome**: I have a dedicated [section about browsers](#Browser) on this article. In any case [Mull](https://f-droid.org/en/packages/us.spotco.fennec_dos/) is a great privacy friendly browser and my recommendation.
- **Gmail** (email client): [K-9 Mail](https://f-droid.org/en/packages/com.fsck.k9/) is IMHO the best mail client for Android. I won't recommend any email provider, but you can look at [PrivacyTools' recommendations](https://privacytools.io/providers/email/)
- **Google Maps**: [OsmAnd](https://f-droid.org/en/packages/net.osmand.plus/) or [Organic Maps](https://f-droid.org/en/packages/app.organicmaps/). Both of them work great, but I slightly prefer Organic Maps.
- **Google Photos**: [Simple Gallery](https://f-droid.org/en/packages/com.simplemobiletools.gallery.pro/) for an off-line photo and video gallery. If you want "cloud" features, I'd recommend you looking at [Nextcloud](https://nextcloud.com/).
- **Google Drive**: [Nextcloud](https://nextcloud.com/) is a well-known option. You may also like [Seafile](https://www.seafile.com/en/home/). Selfhosting is the preferred option, but you can also pay someone to manage it for you.

### More recommended apps

- **[Conversations](https://f-droid.org/en/packages/eu.siacs.conversations/)**, a user friendly XMPP instant messenger client which supports strong e2e encryption. I highly recommend XMPP over other IM services since it's truly decentralized and you can use various encryption methods. If you don't have an account, [here](https://kill-9.xyz/no_category/xmpp) you have a comparison between various XMPP servers.
- **[QKSMS](https://f-droid.org/en/packages/com.moez.QKSMS/)** as a secure SMS client.
- **[Librera Reader](https://f-droid.org/en/packages/com.foobnix.pro.pdf.reader/)** is a feature-rich document reader supporting PDF, EPUB, MOBI, DjVu, TXT, HTML, DOC, DOCX and much more.
- **[Material Files](https://f-droid.org/en/packages/me.zhanghai.android.files/)** for an excellent file manager with root support.
- **[Note Crypt Pro](https://f-droid.org/en/packages/com.notecryptpro/)** is a note editor that keeps your notes encrypted and secure.
- **[Vinyl Music Player](https://f-droid.org/en/packages/com.poupa.vinylmusicplayer/)** is my favorite music player. It's light, fast and customizable.
- **[TrebleShot](https://f-droid.org/en/packages/com.genonbeta.TrebleShot/)** for phone to phone file sharing.
- **[KDE Connect](https://f-droid.org/en/packages/org.kde.kdeconnect_tp/)** for phone-computer file sharing, notifications sync, multimedia remote and much more.
- **[Tusky](https://f-droid.org/en/packages/com.keylesspalace.tusky/)**, the best Mastodon (fediverse) client for Android.

## Privacy issues and possible solutions

Hereafter I'll start enumerating Android phones' privacy issues and I'll try to provide simple and effective solutions.

## Ad and tracker blockers

There are a few ways of blocking ads and trackers on Android. We have three different methods, so I'll try to do my best explaining each of them.

### Using the hosts file
This method **requires root**, but I consider it **the most effective and reliable** option. [Adaway](https://adaway.org/) is the best solution that uses this method. It has **support for block lists**, so you can search for block lists online and use your favorite ones. The other option using this method would be [Hosts Editor](https://f-droid.org/en/packages/com.nilhcem.hostseditor/) which lets you edit your hosts file manually.

### Using DNS
**No root required**. I consider this method the best for non rooted phones. It consists on changing your DNS servers to one which blocks known ads and trackers. You can change your DNS server on Android's Internet configuraton. Even if you use other method I'd suggest **changing your DNS server because the default might be Google's**, depending on the ROM you're using, **which is a huge privacy issue**. You may use any of the following DNS servers: [BlahDNS](https://blahdns.com/), [DNSforge](https://dnsforge.de/), [LibreDNS](https://libredns.gr/) or [Quad9](https://quad9.org/). If there's an active VPN, it will override Android's DNS setting. So this method **will only work when there's no VPN running**.

### Using a local VPN
**No root required**. I consider this the least recommendable method since it will consume more battery than the previous ones. It consists on an app creating a local VPN that will block ads and trackers while it's active. The most popular choice is [Blokada](https://f-droid.org/en/packages/org.blokada.fem.fdroid/). You may also try [PersonalDNSfilter](https://f-droid.org/en/packages/dnsfilter.android/). or [DNS66](https://f-droid.org/en/packages/org.jak_linux.dns66/). Note that these apps overrides DNS settings, so **the DNS method above would only work when these are inactive**.

## Changing your DNS server

As I mentioned before, depending on the ROM that you use (if it's the preinstalled one then this is for sure), **you might be using Google's DNS servers**. Changing the DNS server on Android is really easy and it can be done on Android's configuration. **So it's highly recommended to move your DNS away from Google**. 

Search for DNS on your phone's configuration and you have to select the option of choosing your own one. Then you can enter whatever DNS server you wish. **Some DNS providers block ads and malware for you**, as explained on the [adblocking section](#Ad and tracker blockers). You may use a DNS that blocks ads even if you use other adblock method.

My recommendations for DNS providers:
- [BlahDNS](https://blahdns.com/) - dot-ch.blahdns.com - blocks ads
- [DNSforge](https://dnsforge.de/) - dnsforge.de - blocks ads
- [Applied Privacy](https://applied-privacy.net//services/dns/) - dot1.applied-privacy.net - doesn't block ads
- [NixNet](https://docs.nixnet.services/NixNet_DNS) - adblock.any.dns.nixnet.xyz - blocks ads
- [LibreDNS](https://libredns.gr/) - dot.libredns.gr - doesn't block ads
- [Quad9](https://quad9.org/) - dns.quad9.net - only blocks malware

## Firewalls

**The use of a firewall is essential** if you want to protect your privacy. It restricts apps' internet access so they won't be able to send telemetry or any other privacy invasive stuff. If you follow this guide then you'll be only installing free software, but just in case I always recommend having a Firewall, because **why would I grant internet access my gallery app?** Or to my music player? They don't need them. And just in case I deny it (actually, I have a whitelist where I select manually which apps can access the internet).

- **AFWall+**: It is by far the best Firewall for Android. It's a frontend for iptables, so **it requires root**. It's main advantage is that **it utilizes the kernel iptables, so it completely blocks apps phoning home**. It can not be bypassed. The UI is fairly straightforward to use while being powerful and having a lot of useful features. Highly recommended.
- **[Netguard](https://f-droid.org/en/packages/eu.faircode.netguard/)**: Netguard is a well-known firewall for Android. **It doesn't require root** since it creates a local VPN which it uses to grant or deny internet access. If your phone is rooted it's far better to use AFWall+ but if it isn't, Netguard is the best choice.

I suggest using your firewall in **whitelist mode and only grant access to the apps that absolutely need it**. This way you'll minimize the number of apps phoning home. If an app doesn't require internet access, then leave it on the blacklist. Even if it is an app from F-Droid. Deny internet access, just in case. **Being cautious doesn't hurt**.

PS. **If you use AFWall+, be sure to enable the option "fix startup data leak"** in the experimental settings. If it doesn't let you enable, you have to change the startup directory path (above). This will prevent any leak on startup.

## VPN

I don't believe in VPNs. You're basically blind trusting a provider that promises to not log you. When using a VPN, your entire traffic goes unencrypted through their servers. This is how VPNs works. 

Instead of trusting a VPN provider, we will use the [Tor network](https://www.torproject.org/) acting as a proxy for the apps that we select, eg our RSS feed reader or a mail client.

- **[Orbot](https://guardianproject.info/apps/org.torproject.android/)**: Orbot acts a proxy routing your traffic through the Tor network, encrypting it and hiding your real identity.

## Browser

Well, this is a controversial topic. But after my tests and trying various browsers, I think that it is safe to say that [Mull](https://f-droid.org/en/packages/us.spotco.fennec_dos/) is one of the best options for Android.

Mull uses many features from the [Tor Uplift Project](https://wiki.mozilla.org/Security/Tor_Uplift) and preferences from the [Arkenfox's user.js](https://github.com/arkenfox/user.js). This means that **Mull is "a hardened Firefox" for Android**. It could be said that Mull is the [Librewolf](https://librewolf-community.gitlab.io/) for Android. It supports addons so **you should install uBlock Origin and Decentraleyes for additional protection** against trackers. I'd also recommend disabling JavaScript by default using UBO, but that is up to you.

Other recommended browser is the [Tor browser](https://www.torproject.org/) which focus on anonymity. It may be too slow for a daily driver and that's why I recommend Mull. 

### Search engine

I've already talked about search engines on the blog and I might write a more detailed article covering the topic on the future. Anyway, I'll give you the few options that I recommend (in order of preference):

- **[Searx](https://searx.me/)**: **The best option in my opinion**. A completely free as in freedom and self-hostable meta-search engine that just works. You can find a list of public instances [here](https://searx.space/) or you can host it yourself. It works without JavaScript. Searx **obtains it results from a variety of search engines** (Bing, DDG, Yandex, Google, etc) and you can configure it to suit your preferences.
- **[Whoogle](https://github.com/benbusby/whoogle-search)**: Another self-hostable, free as in freedom meta-search engine. But unlike Searx which extracts it results from various search engines Whoogle gets Google's results. So it's like searching on Google but without ads, trackers, cookies, JavaScript or any other Google's garbage. **Just a privacy friendly way of obtaining Google's results**. It's perfect for those of you who don't leave Google because it gives you "quality" results. You can find public instances [here](https://github.com/benbusby/whoogle-search#public-instances) or host your own.
- **[Metager](https://metager.org/)**: Metager is a libre (meta) search engine. And I say meta in parenthesis because **it also has it's own index, but the vast majority of the results are obtained from Bing**. It's run by a non-profit and it is fully free. Although they don't provide images to self-host the code is available so it is theoretically possible.
- **[Yacy](https://yacy.net/)**: It's a **peer to peer free search engine which doesn't rely on any central server**. Rather than relying on the proprietary software of a corporation, your search engine is run by many private computers which aren't under the control of any one company or individual. Web Search by the people, for the people. It is totally decentralized. You'd have to **install it on a computer** in order to use it, since it's peer to peer. That's why it isn't in a higher position.

## Microphone and camera

Proprietary apps tend to require absurd amounts of permissions. Those includes microphone and camera, which can record really sensitive identifiable data. Even if we don't use proprietary apps, we still have some proprietary firmware (unless you're a Replicant) so it may be a good idea to block both your camera and your microphone.

The idea solution would be to have hardware kill switches. But almost no device is made with these (because corps don't want us to have any kind of privacy, oc). So we'll have to use some apps as a workaround. I'll give you a bunch of options:

If you don't want to trust the software or you think that the spyware acts in a lower level, then you can use physical mic blockers if you have a 3.5 jack. **Covering your camera with a piece of adhesive paper** is easy and it should be done much more on phones. I don't understand why people cover the camera on their laptops but not on their phones.

### Apps that alert you when the cam/mic is in use

Being alerted when an app access your camera or your microphone is a good way to know if you're being spied on. Various apps offer this:

- [Vigilante](https://f-droid.org/en/packages/com.crazylegend.vigilante/) It alerts you when the microphone/camera is being used with a dot or a notification.
- [Privacy Indicator](https://f-droid.org/en/packages/com.nitish.privacyindicator/) Same as above but can also alert you about location being used.

### Block ultrasonic cross-device trackers and Google Assistant

- **[PilferShush Jammer](https://f-droid.org/en/packages/cityfreqs.com.pilfershushjammer/)**: Features a passive jammer technique that engages the microphone thereby **blocking other apps from using it**. It has a Passive blocker which "hijacks" your microphone and **will prevent any other app from using it**. It also has an Active blocker that will make blank noise so the microphone can't record anything. 

This application is also perfect **to prevent [ultrasonic cross-device tracking](https://www.wired.com/2016/11/block-ultrasonic-signals-didnt-know-tracking/). It consists on embedding high-frequency tones that are inaudible to humans in advertisements, web pages, and even physical locations like retail stores**. These ultrasound "beacons" emit their audio sequences with speakers, and almost any device microphone can detect the signal and know what ads you've seen

And a **killer feature for those of you who haven't flashed a degoogled ROM**, it has the "Voice Assistant Jammer" which is aimed to substitute Google's Assistant so it is completely neutralized, even if you can't uninstall it.

## Erase image metadata

Removing metadata of your images is really recommended if you're going to share them through email, social media or by instant messaging. Metadata may contain sensitive information as location, date, etc.

With these apps you can remove metadata before sharing your images.
- **[Scrambled Exif](https://f-droid.org/en/packages/com.jarsilio.android.scrambledeggsif/)**: removes metadata of your pictures before you share them, directly from the share button.
- **[ImagePipe](https://f-droid.org/en/packages/de.kaffeemitkoffein.imagepipe/)** it's a more complete solutions for those who need more than just sharing the app without exif data (metadata). It can reduce image size and it may also be used as an image editor.

## Bluetooth mitigation

Bluetooth is a highly insecure protocol. But not only that, it is also a [privacy nightmare](https://mashable.com/article/bluetooth-is-bad). [Like The New York Times have reported](https://www.nytimes.com/interactive/2019/06/14/opinion/bluetooth-wireless-tracking-privacy.html) many stores use Bluetooth beacons to track the location of shoppers. **Bluetooth beacons can track your location accurately from a range of inches to about 50 meters**. They use very little energy, and they work well indoors, being able to track the location down to the centimeter. That has made them popular. The information they collect is sold to advertisers, who then use it to build data profiles.

In a nutshell, **keeping Bluetooth enabled on your phone at all times makes you vulnerable to potential hacks, abuses, and privacy violations. The solution is simple: Don't use it**.

If you want to use it even after knowing that, I have an **app for you that will help you to have Bluetooth disabled** when you aren't using it. It is **[Greentooth](https://f-droid.org/en/packages/com.smilla.greentooth/), an automatic bluetooth disabler**. On top of the privacy benefits, it will also save you battery. Great, isn't it?

## Prevent apps from auto-starting on boot

If you've installed a lot of apps and your phone slows down, you may use [Autostarts](https://f-droid.org/en/packages/com.elsdoerfer.android.autostarts/). But it isn't here because it helps preventing your phone slowing down. This apps **can prevent proprietary and/or non trusted apps from starting at boot**. It will definitely will be useful while trying to be private.

It'll let you manage which apps automatically start while startup and right after startup. Of course, **it requires root access**. But it's a highly recommended tool, since it gives you more control over your device startup.

## Freezing apps running in background

Along with preventing apps from auto-starting, [SuperFreezZ](https://f-droid.org/en/packages/superfreeze.tool.android/) can freeze apps running in background and which apps should auto-freeze after a certain time. **Autostarts + SuperFreezZ gives us full control over when an app may or may not run**. 

## Changing the default keyboard

I think that at this point it isn't necessary to be said, but **Google's GBoard is a keylogger**. Don't use it, under any circumstance.

Instead install one of the great free as in freedom keyboards available on F-Droid:
- [OpenBoard](https://f-droid.org/en/packages/org.dslul.openboard.inputmethod.latin/)
- [AnySoftKeyboard](https://f-droid.org/en/packages/com.anysoftkeyboard.languagepack.hebrew/)
- [FlorisBoard](https://f-droid.org/en/packages/dev.patrickgold.florisboard/)

Then set it as your keyboard on your configuration. Don't forget to replace the default spell check with your keyboard's too (It's a different option on the config)

Remember that **replacing GBoard with another proprietary keyboard doesn't make any sense**. So please install OpenBoard since it is a great keyboard that will respect your privacy. Anyway, you should blacklist it on your firewall, just in case.

## Restricting access to your clipboard

Restricting access to the clipboard can't be done by default on Android. This is a privacy issue, especially if you use proprietary apps. Even I that use exclusively free software I feel insecure and violated allowing every single app to read my clipboard.

However this can't be easily fixed. Fortunately, root users will be able to restrict clipboard access easily using [XPrivacyLua](https://f-droid.org/en/packages/eu.faircode.xlua/). You'll need **root + xposed**. Then you'll be able to restrict a great variety of permissions. For more information, go to the next section.

PS. The only apps that have clipboard access on my phone are my IM app, my browser and my note taking app. **Restrict clipboard access to every app** but the absolutely necessary.

## ADVANCED: Limiting apps permissions in depth

It is possible to limit app permissions far more than using the default Android's permission manager. Actually, it **allows to manage granular app permissions not visible normally**. You can disallow permission for obtaining other apps that you have installed, accessing your clipboard, seeing phone accounts and other cool stuff that maximizes our privacy and the control over our devices.

You'll need **root access and Xposed**. Then you'll be able to use [XPrivacyLua](https://f-droid.org/en/packages/eu.faircode.xlua/) for managing every app permissions far more in depth than using the permission manager. **A killer feature for me is restricting access to the clipboard**, since we constantly copy and past sensible information, like passwords and usernames, especially if we use a password manager, and by default any app can read your clipboard. XPrivacyLua fixes that.

And one of my favorite things of rooting is that **root + xposed allow us to spoof the following values: Imei, Hardware ID, Mac Address**, Mac Bssid, Mac Sside, Bluetooth Mac, Android Id, Sim serial ID, Sim sub IDs and mobile number.

### The definitive way of sandboxing proprietary software

Using XPrivacyLua + [Insular](#Isolating proprietary software) you can completely isolate proprietary or any non trusted apps. You should restrict every permission using XPrivacyLua but the essentials for the app. 

In addition, using [Autostarts and SuperFreezZ](#Prevent apps from auto-starting on boot) means that you've finally reached full control over what is executed on your phone.

And again, don't install proprietary software under any circumstances. I'm only giving this advice because some people might not have other option.

## Faraday bag

A Faraday bag is a copper foil-powered enclosure which blocks all radio signals, from Wifi and Bluetooth to the phone network and the GPS. Faraday bags play the role of a real “airplane mode”, assuring that no connection will be made from the device. 

Of course that you won't be able to receive phone calls, but that's the purpose of using a Faraday bag: **turning your phone into a useless paperweight**.

Would I consider necessary a Faraday bag even after all the improvements and modifications we've done? Of course. I'll explain you why.

In case you didn't know it, [these are the methods used for tracking your phone location](https://en.wikipedia.org/wiki/Mobile_phone_tracking). So even if we blocked Wifi based tracking, Bluetooth based tracking and GPS signal with software, **it's still possible to track us using [cell towers](https://en.wikipedia.org/wiki/Cell_tower) [triangulation](https://en.wikipedia.org/wiki/Triangulation)**. 

This is the consequence of being connected to the [Global System for Mobile Communications](https://en.wikipedia.org/wiki/GSM) (GSM). If you have a phone service in order to receive calls and cellular data, it's mandatory to be connected to the GSM. **There's nothing you can do about it. The only solution is using a Faraday bag** when we truly don't want to be tracked, because even with the phone powered off it may still be triangulated. 

It's almost magic, **millions of euros worth of surveillance systems can be defeated whenever you put your electronic devices inside these little bags**.

## Conclusion

If you've completed this guide, you should be proud. Your "smartphone" is clean and private. Enjoy your freedom :)

Following the methodology I explained on the introduction **every app listed on this guide is [free as in freedom](http://www.gnu.org/philosophy/free-sw.html)**. If I'm mistaken please let me know immediately. I've checked everything twice and almost all apps are from F-Droid, so it should be OK.

If you have any doubt or suggestion, feel free to ask me. I'll try to reply as soon as possible.

Thanks for reading


