tags: Comments
title: Blog update
author: Werwolf
date: 2022-02-05
aside: Brief post about the changes on this blog

After a long time, I'm updating the blog. This is not the first post after a long time, since the
[blockchain one](/on-blockchain.md) goes first (it was written months ago, but not published until
now), but I wanted to give a short explanation about the changes on my site.

## Changes to the blog

* Perhaps the biggest and more noticeable change is that I've moved my blog to my own domain. This was planned for so long
but I didn't make the change until now. 

* I'm hosting this blog from a Raspberry Pi in my home,
so you might see some down time. If you were curious, the Pi is running NetBSD.

* I switched from Hugo to Sblg as my static site generator. [Sbgl](https://kristaps.bsd.lv/sblg) is a
simple UNIX tool written in C for creating static websites, using POSIX make.

* The [feed](atom.xml) uses now the atom specification, instead of RSS. After comparing them I think
that the atom standard is superior. Anyway, every RSS reader is compatible with both of them.

* There's now **a comment section**. More on that later.

* The general license is now [CC0](/pages/license.txt). I felt like CC-BY-SA was too restrictive and that if I write this
blog is for two reasons: The main reason is for myself, as I enjoy writing, and the second one is that I share
information about things that I consider important. Since I believe that information should be
free, CC0 seems to be a better choice.

* I've reduced the CSS to a really simple file, the bare minimum to make the page readable and a few
minor aesthetic changes (a dark background and red hyperlinks).

## Comments

I didn't know if I really wanted to implement comments or how to do it. I found lots of solutions
for comments on static sites generator, but no one was what I was looking for.

I wanted something completely static, that could be archived in the same html file with the article,
that didn't make external connections, that didn't use JavaScript nor complex CSS and that it was
easy to manage.

Since I didn't find anything like that and given that I won't receive many comments, I opted to go
with the easy route: simple email comments, inspired by [Tdarb](https://tdarb.org/). I simply put a mailto link at the end of every article and
I'll manually add the comments to the markdown file of each post. It's also a great way of dealing
with spam.

Maybe in the future I'll write a script to automate the thing a bit, but for now I don't mind
adding a few comments by hand.

### Writing a comment

It's as easy as clicking on the link at the end of every post, writing the email and sending it. The
only requisite is to write it on plain text. By default, I'll put the name on your email as the name of the
commenter. If you want me to put other nick or just post it anonymous, just let me know it on the
email. Easy, isn't it?

### Removing comments

Since the comments will be on the html files and the git repository for this blog is public, it will
be necessary to remove the git history in order to remove one comments. 

When you comment, keep in
mind that it will be made public and while it is possible to delete them, it's quite annoying. I'm
not worried about this situation though, because there won't be any personal information related to
the comment other than the nick they give me on the email. And if I had to remove a comment, I could
always update a git repo with the history removed and keep a local copy with the whole history, just
in case.

## More frequent new posts

I hope to write more now that I've moved the blog to my server. I've been waiting for this before
writing anything, except for the web3 article that I've slowly been waiting for a long time. We will
see.
