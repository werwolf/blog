tags: Hardware Consumerism Minimalism
title: You don't need a new computer
author: Werwolf
date: 2021-04-27
aside: Do you really need the latest hardware? A 13 year old computer is able to perform most of your needs

Nowadays almost everyone owns a computer. Living in a world where consumerism rules, such an essential tool in our lives have created a very attractive market . Companies started making computers more difficult to repair when they realized that the average user bought a new computer whenever an unsignificant part of their laptop broke. 

This have had numerous consequences, I'd like to bring focus to the following facts:

- Computers are now harder to repair than ever before. Companies profit when you buy a new computer so they don't want you to have your hardware repaired.
- There is an increasing trend towards making computers less and less durable. For companies it's more profitable to build low quality products that will last less time. That way they assure that you'll continue buying new products. Even further, they make their hardware with a limited useful life. This is known as programmed obsolescence and it's a huge issue for consumers. Planned obsolescence works even better in an oligopoly where 3-4 corps control the market. And yes, this is happening right now.
- Producing such a huge amount of e-wast is dangerously damaging our planet.

## New computers are more fragile

As I stated before, it's better for companies to make computers impossible to repair or less durable so they'll sell you another one. And if it's possible, a more expensive one marketing it as "more durable" or as "next gen tech". 

Apple might be the worst company in this aspect. Every Apple product has been purposely made to make it hard to fix, even if it's a very simple problem, that would be easily fixable in a more ethical product. They even use special screws, just to make sure that normal people can't open their devices with a normal screwdriver. If a third party wants to officially repair their repugnant hardware, this third party will have to pay an absurdly expensive license to Apple. Now every other manufacturer is replicating Apple's terrible techniques.

## Buying new hardware damages our environment

We make too much e-waste. This is a known problem by everyone, but frequently we don't realize how important it is and that we need to stop it now. More than 50 million tons of e-waste are produced every year. And this is only getting worse.

For getting an idea of this, if you put every blue whale alive today on one side of a scale and on year of US e-waste on the other, the e-waste would be heavier. Taking into account **exclusively US e-waste**. 

Using your (relatively) old computers help to keep this waste out of environment. It also helps to conserve natural resources. 

## Modern computers are monitoring devices

It seems that this issue is well known between IT people. But the average user doesn't know or doesn't give importance to this increasing problem. Massive surveillance is a violation of human rights that isn't showed in any news channel. 

Even if you get rid of Micro$oft Window$ and you install your favorite free operating system, **you'll be still being spied by megacorps and governments**. 

### How is this even possible?

Of course that this is possible. Apart of constantly being tracked while you browse the web, the problem goes much deeper than that. 

Every single Intel processor since 2008 has included an autonomous subsystem inside of your processor, which is oftenly known as [Intel ME](https://en.wikipedia.org/wiki/Intel_Management_Engine). I must say that AMD has the exact same thing on their machines but they call it in a different way ([AMD PSP](https://en.wikipedia.org/wiki/AMD_Platform_Security_Processor))

**This subsystem can read your memory and connect to the internet. This is, basically, a permanent backdoor**. In some older computers you can remove this other processor and replace the proprietary BIOS in you PC with [Libreboot](https://libreboot.org/). 

>Libreboot is freedom-respecting boot firmware, initialising the hardware and booting an operating system. This replaces the proprietary BIOS/UEFI boot firmware found in computers.

## So, do you really need a modern computer?

I don't think so. And I promise you that you probably don't need one. IMO **more than the 90% of people could be using a computer from 2008 or before** with a nice user experience. The need for a newer machine is only for people who do professional hardware demanding video editing, compile massive operating systems or enormous programs constantly, use bloated programs or other horrible designed software. 

If everything that you do in your computer is watching videos on the internet, browsing a few sites and using a word processor, **then you don't have the necessity to use recent hardware**. 

I know what are you going to say. "My laptop runs slow". That's a Windows problem. Our friend Micro$oft, who profits with every laptop sale since manufacturers need to buy Windows licenses, makes Windows heavier and heavier with every update. This makes computers slow and forces users to buy new hardware periodically even if their machines would work flawlessly with a true OS and not with Microsoft's crap. 

### The abusive prices of new computers

Nowadays computers have absurd prices. A computer for a normal person who doesn't need high end hardware for professional use **should cost less than 100 euros**. Yeah, you've read correctly, less than 100 euros. 

The laptop that I use everyday cost me that quantity in the second hand market. If you're buying first hand products, you're promoting e-waste creation. 

## Conclusion

**You don't need a new computer**. I'm using a laptop that is more than a decade old as my everyday machine. You can continue using your old laptop without any problem. **Don't fall into consumerism, that's what they want**.
